// Generated from BKIT.g4 by ANTLR 4.9.2


 
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link BKITParser}.
 */
public interface BKITListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link BKITParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(BKITParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link BKITParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(BKITParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link BKITParser#var_declare}.
	 * @param ctx the parse tree
	 */
	void enterVar_declare(BKITParser.Var_declareContext ctx);
	/**
	 * Exit a parse tree produced by {@link BKITParser#var_declare}.
	 * @param ctx the parse tree
	 */
	void exitVar_declare(BKITParser.Var_declareContext ctx);
	/**
	 * Enter a parse tree produced by {@link BKITParser#id_init}.
	 * @param ctx the parse tree
	 */
	void enterId_init(BKITParser.Id_initContext ctx);
	/**
	 * Exit a parse tree produced by {@link BKITParser#id_init}.
	 * @param ctx the parse tree
	 */
	void exitId_init(BKITParser.Id_initContext ctx);
	/**
	 * Enter a parse tree produced by {@link BKITParser#init_variable}.
	 * @param ctx the parse tree
	 */
	void enterInit_variable(BKITParser.Init_variableContext ctx);
	/**
	 * Exit a parse tree produced by {@link BKITParser#init_variable}.
	 * @param ctx the parse tree
	 */
	void exitInit_variable(BKITParser.Init_variableContext ctx);
	/**
	 * Enter a parse tree produced by {@link BKITParser#func_declare}.
	 * @param ctx the parse tree
	 */
	void enterFunc_declare(BKITParser.Func_declareContext ctx);
	/**
	 * Exit a parse tree produced by {@link BKITParser#func_declare}.
	 * @param ctx the parse tree
	 */
	void exitFunc_declare(BKITParser.Func_declareContext ctx);
	/**
	 * Enter a parse tree produced by {@link BKITParser#func_para}.
	 * @param ctx the parse tree
	 */
	void enterFunc_para(BKITParser.Func_paraContext ctx);
	/**
	 * Exit a parse tree produced by {@link BKITParser#func_para}.
	 * @param ctx the parse tree
	 */
	void exitFunc_para(BKITParser.Func_paraContext ctx);
	/**
	 * Enter a parse tree produced by {@link BKITParser#func_body}.
	 * @param ctx the parse tree
	 */
	void enterFunc_body(BKITParser.Func_bodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link BKITParser#func_body}.
	 * @param ctx the parse tree
	 */
	void exitFunc_body(BKITParser.Func_bodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link BKITParser#all_literal}.
	 * @param ctx the parse tree
	 */
	void enterAll_literal(BKITParser.All_literalContext ctx);
	/**
	 * Exit a parse tree produced by {@link BKITParser#all_literal}.
	 * @param ctx the parse tree
	 */
	void exitAll_literal(BKITParser.All_literalContext ctx);
	/**
	 * Enter a parse tree produced by {@link BKITParser#single_lit}.
	 * @param ctx the parse tree
	 */
	void enterSingle_lit(BKITParser.Single_litContext ctx);
	/**
	 * Exit a parse tree produced by {@link BKITParser#single_lit}.
	 * @param ctx the parse tree
	 */
	void exitSingle_lit(BKITParser.Single_litContext ctx);
	/**
	 * Enter a parse tree produced by {@link BKITParser#array_lit}.
	 * @param ctx the parse tree
	 */
	void enterArray_lit(BKITParser.Array_litContext ctx);
	/**
	 * Exit a parse tree produced by {@link BKITParser#array_lit}.
	 * @param ctx the parse tree
	 */
	void exitArray_lit(BKITParser.Array_litContext ctx);
	/**
	 * Enter a parse tree produced by {@link BKITParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(BKITParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BKITParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(BKITParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link BKITParser#exp2}.
	 * @param ctx the parse tree
	 */
	void enterExp2(BKITParser.Exp2Context ctx);
	/**
	 * Exit a parse tree produced by {@link BKITParser#exp2}.
	 * @param ctx the parse tree
	 */
	void exitExp2(BKITParser.Exp2Context ctx);
	/**
	 * Enter a parse tree produced by {@link BKITParser#index_exp}.
	 * @param ctx the parse tree
	 */
	void enterIndex_exp(BKITParser.Index_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link BKITParser#index_exp}.
	 * @param ctx the parse tree
	 */
	void exitIndex_exp(BKITParser.Index_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link BKITParser#statement_list}.
	 * @param ctx the parse tree
	 */
	void enterStatement_list(BKITParser.Statement_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link BKITParser#statement_list}.
	 * @param ctx the parse tree
	 */
	void exitStatement_list(BKITParser.Statement_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link BKITParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(BKITParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link BKITParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(BKITParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link BKITParser#assignment}.
	 * @param ctx the parse tree
	 */
	void enterAssignment(BKITParser.AssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link BKITParser#assignment}.
	 * @param ctx the parse tree
	 */
	void exitAssignment(BKITParser.AssignmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link BKITParser#if_stmt}.
	 * @param ctx the parse tree
	 */
	void enterIf_stmt(BKITParser.If_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link BKITParser#if_stmt}.
	 * @param ctx the parse tree
	 */
	void exitIf_stmt(BKITParser.If_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link BKITParser#for_stmt}.
	 * @param ctx the parse tree
	 */
	void enterFor_stmt(BKITParser.For_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link BKITParser#for_stmt}.
	 * @param ctx the parse tree
	 */
	void exitFor_stmt(BKITParser.For_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link BKITParser#while_stmt}.
	 * @param ctx the parse tree
	 */
	void enterWhile_stmt(BKITParser.While_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link BKITParser#while_stmt}.
	 * @param ctx the parse tree
	 */
	void exitWhile_stmt(BKITParser.While_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link BKITParser#do_stmt}.
	 * @param ctx the parse tree
	 */
	void enterDo_stmt(BKITParser.Do_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link BKITParser#do_stmt}.
	 * @param ctx the parse tree
	 */
	void exitDo_stmt(BKITParser.Do_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link BKITParser#funcCall}.
	 * @param ctx the parse tree
	 */
	void enterFuncCall(BKITParser.FuncCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link BKITParser#funcCall}.
	 * @param ctx the parse tree
	 */
	void exitFuncCall(BKITParser.FuncCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link BKITParser#ret_stmt}.
	 * @param ctx the parse tree
	 */
	void enterRet_stmt(BKITParser.Ret_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link BKITParser#ret_stmt}.
	 * @param ctx the parse tree
	 */
	void exitRet_stmt(BKITParser.Ret_stmtContext ctx);
}