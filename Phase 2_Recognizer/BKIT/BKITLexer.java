// Generated from BKIT.g4 by ANTLR 4.9.2



import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BKITLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.9.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		COMMENT=1, ID=2, BODY=3, BREAK=4, CONT=5, DO=6, ELSE=7, ELSEIF=8, ENDBODY=9, 
		ENDIF=10, ENDFOR=11, ENDWHILE=12, FOR=13, FUNC=14, IF=15, PARA=16, RETURN=17, 
		THEN=18, VAR=19, WHILE=20, ENDDO=21, ADD=22, ADDF=23, SUB=24, SUBF=25, 
		MUL=26, MULF=27, DIV=28, DIVF=29, MOD=30, ASGN=31, NOT=32, AND=33, OR=34, 
		EQUAL=35, NEQUAL=36, LESS=37, GREATER=38, SEQUAL=39, LEQUAL=40, NEQUALF=41, 
		LESSF=42, GREATERF=43, SEQUALF=44, LEQUALF=45, RBRACKET_L=46, RBRACKET_R=47, 
		SBRACKET_L=48, SBRACKET_R=49, SEMI=50, DOT=51, COMMA=52, COLON=53, CBRACKET_L=54, 
		CBRACKET_R=55, INT_LIT=56, FLOAT_LIT=57, BOOL_LIT=58, STRING_LIT=59, WS=60, 
		UNCLOSE_STRING=61, ILLEGAL_ESCAPE=62, UNTERMINATED_COMMENT=63, ARRAY_LIT=64;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"COMMENT", "ID", "BODY", "BREAK", "CONT", "DO", "ELSE", "ELSEIF", "ENDBODY", 
			"ENDIF", "ENDFOR", "ENDWHILE", "FOR", "FUNC", "IF", "PARA", "RETURN", 
			"THEN", "VAR", "WHILE", "TRUE", "FALSE", "ENDDO", "ADD", "ADDF", "SUB", 
			"SUBF", "MUL", "MULF", "DIV", "DIVF", "MOD", "ASGN", "NOT", "AND", "OR", 
			"EQUAL", "NEQUAL", "LESS", "GREATER", "SEQUAL", "LEQUAL", "NEQUALF", 
			"LESSF", "GREATERF", "SEQUALF", "LEQUALF", "RBRACKET_L", "RBRACKET_R", 
			"SBRACKET_L", "SBRACKET_R", "SEMI", "DOT", "COMMA", "COLON", "CBRACKET_L", 
			"CBRACKET_R", "DEC_LIT", "HEX_LIT", "OCT_LIT", "INT_LIT", "DecimalPart", 
			"Digits", "ExponentPart", "FLOAT_LIT", "BOOL_LIT", "LegalEscape", "StringElement", 
			"STRING_LIT", "WS", "UNCLOSE_STRING", "ILLEGAL_ESCAPE", "UNTERMINATED_COMMENT", 
			"ARRAY_LIT"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, "'Body'", "'Break'", "'Continue'", "'Do'", "'Else'", 
			"'ElseIf'", "'EndBody'", "'EndIf'", "'EndFor'", "'EndWhile'", "'For'", 
			"'Function'", "'If'", "'Parameter'", "'Return'", "'Then'", "'Var'", "'While'", 
			"'EndDo'", "'+'", "'+.'", "'-'", "'-.'", "'*'", "'*.'", "'\\'", "'\\.'", 
			"'%'", "'='", "'!'", "'&&'", "'||'", "'=='", "'!='", "'<'", "'>'", "'<='", 
			"'>='", "'=/='", "'<.'", "'>.'", "'<=.'", "'>=.'", "'('", "')'", "'['", 
			"']'", "';'", "'.'", "','", "':'", "'{'", "'}'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "COMMENT", "ID", "BODY", "BREAK", "CONT", "DO", "ELSE", "ELSEIF", 
			"ENDBODY", "ENDIF", "ENDFOR", "ENDWHILE", "FOR", "FUNC", "IF", "PARA", 
			"RETURN", "THEN", "VAR", "WHILE", "ENDDO", "ADD", "ADDF", "SUB", "SUBF", 
			"MUL", "MULF", "DIV", "DIVF", "MOD", "ASGN", "NOT", "AND", "OR", "EQUAL", 
			"NEQUAL", "LESS", "GREATER", "SEQUAL", "LEQUAL", "NEQUALF", "LESSF", 
			"GREATERF", "SEQUALF", "LEQUALF", "RBRACKET_L", "RBRACKET_R", "SBRACKET_L", 
			"SBRACKET_R", "SEMI", "DOT", "COMMA", "COLON", "CBRACKET_L", "CBRACKET_R", 
			"INT_LIT", "FLOAT_LIT", "BOOL_LIT", "STRING_LIT", "WS", "UNCLOSE_STRING", 
			"ILLEGAL_ESCAPE", "UNTERMINATED_COMMENT", "ARRAY_LIT"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public BKITLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "BKIT.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	@Override
	public void action(RuleContext _localctx, int ruleIndex, int actionIndex) {
		switch (ruleIndex) {
		case 68:
			STRING_LIT_action((RuleContext)_localctx, actionIndex);
			break;
		case 70:
			UNCLOSE_STRING_action((RuleContext)_localctx, actionIndex);
			break;
		case 71:
			ILLEGAL_ESCAPE_action((RuleContext)_localctx, actionIndex);
			break;
		}
	}
	private void STRING_LIT_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 0:
			setText(getText().substring(1, getText().length()-1));
			break;
		}
	}
	private void UNCLOSE_STRING_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 1:
			setText(getText().substring(1, getText().length()-1));
			break;
		}
	}
	private void ILLEGAL_ESCAPE_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 2:
			setText(getText().substring(1, getText().length()-1));
			break;
		}
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2B\u0217\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I"+
		"\tI\4J\tJ\4K\tK\3\2\3\2\3\2\3\2\7\2\u009c\n\2\f\2\16\2\u009f\13\2\3\2"+
		"\3\2\3\2\3\2\3\2\3\3\3\3\7\3\u00a8\n\3\f\3\16\3\u00ab\13\3\3\4\3\4\3\4"+
		"\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3"+
		"\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n"+
		"\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f"+
		"\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\17"+
		"\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\21\3\21\3\21"+
		"\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\22\3\22"+
		"\3\23\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\25\3\25\3\25\3\25\3\25"+
		"\3\25\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3\27\3\27\3\27\3\27\3\30\3\30"+
		"\3\30\3\30\3\30\3\30\3\31\3\31\3\32\3\32\3\32\3\33\3\33\3\34\3\34\3\34"+
		"\3\35\3\35\3\36\3\36\3\36\3\37\3\37\3 \3 \3 \3!\3!\3\"\3\"\3#\3#\3$\3"+
		"$\3$\3%\3%\3%\3&\3&\3&\3\'\3\'\3\'\3(\3(\3)\3)\3*\3*\3*\3+\3+\3+\3,\3"+
		",\3,\3,\3-\3-\3-\3.\3.\3.\3/\3/\3/\3/\3\60\3\60\3\60\3\60\3\61\3\61\3"+
		"\62\3\62\3\63\3\63\3\64\3\64\3\65\3\65\3\66\3\66\3\67\3\67\38\38\39\3"+
		"9\3:\3:\3;\3;\3;\7;\u0188\n;\f;\16;\u018b\13;\5;\u018d\n;\3<\3<\3<\3<"+
		"\7<\u0193\n<\f<\16<\u0196\13<\3=\3=\3=\3=\7=\u019c\n=\f=\16=\u019f\13"+
		"=\3>\3>\3>\5>\u01a4\n>\3?\3?\7?\u01a8\n?\f?\16?\u01ab\13?\3@\6@\u01ae"+
		"\n@\r@\16@\u01af\3A\3A\5A\u01b4\nA\3A\3A\3B\3B\3B\5B\u01bb\nB\3B\5B\u01be"+
		"\nB\3C\3C\5C\u01c2\nC\3D\3D\3E\3E\3E\3E\3E\5E\u01cb\nE\3F\3F\7F\u01cf"+
		"\nF\fF\16F\u01d2\13F\3F\3F\3F\3G\6G\u01d8\nG\rG\16G\u01d9\3G\3G\3H\3H"+
		"\7H\u01e0\nH\fH\16H\u01e3\13H\3H\5H\u01e6\nH\3H\3H\3I\3I\7I\u01ec\nI\f"+
		"I\16I\u01ef\13I\3I\3I\3I\3I\5I\u01f5\nI\3I\3I\3J\3J\3J\3J\3J\3J\7J\u01ff"+
		"\nJ\fJ\16J\u0202\13J\3K\3K\3K\3K\3K\3K\3K\3K\3K\3K\3K\7K\u020f\nK\fK\16"+
		"K\u0212\13K\5K\u0214\nK\3K\3K\3\u009d\2L\3\3\5\4\7\5\t\6\13\7\r\b\17\t"+
		"\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\25)\26+\2"+
		"-\2/\27\61\30\63\31\65\32\67\339\34;\35=\36?\37A C!E\"G#I$K%M&O\'Q(S)"+
		"U*W+Y,[-]._/a\60c\61e\62g\63i\64k\65m\66o\67q8s9u\2w\2y\2{:}\2\177\2\u0081"+
		"\2\u0083;\u0085<\u0087\2\u0089\2\u008b=\u008d>\u008f?\u0091@\u0093A\u0095"+
		"B\3\2\26\3\2c|\6\2\62;C\\aac|\3\2\63;\3\2\62;\4\2ZZzz\4\2\63;CH\4\2\62"+
		";CH\4\2QQqq\3\2\639\3\2\629\4\2GGgg\4\2--//\t\2))^^ddhhppttvv\3\2^^\7"+
		"\2\f\f\17\17$$))^^\5\2\13\f\17\17\"\"\4\3\f\f\17\17\3\2))\3\2$$\3\2,,"+
		"\2\u022b\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2"+
		"\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27"+
		"\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2"+
		"\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2/\3\2\2\2\2\61\3\2\2"+
		"\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2"+
		"\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2"+
		"\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2W"+
		"\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2\2\2\2a\3\2\2\2\2c\3\2"+
		"\2\2\2e\3\2\2\2\2g\3\2\2\2\2i\3\2\2\2\2k\3\2\2\2\2m\3\2\2\2\2o\3\2\2\2"+
		"\2q\3\2\2\2\2s\3\2\2\2\2{\3\2\2\2\2\u0083\3\2\2\2\2\u0085\3\2\2\2\2\u008b"+
		"\3\2\2\2\2\u008d\3\2\2\2\2\u008f\3\2\2\2\2\u0091\3\2\2\2\2\u0093\3\2\2"+
		"\2\2\u0095\3\2\2\2\3\u0097\3\2\2\2\5\u00a5\3\2\2\2\7\u00ac\3\2\2\2\t\u00b1"+
		"\3\2\2\2\13\u00b7\3\2\2\2\r\u00c0\3\2\2\2\17\u00c3\3\2\2\2\21\u00c8\3"+
		"\2\2\2\23\u00cf\3\2\2\2\25\u00d7\3\2\2\2\27\u00dd\3\2\2\2\31\u00e4\3\2"+
		"\2\2\33\u00ed\3\2\2\2\35\u00f1\3\2\2\2\37\u00fa\3\2\2\2!\u00fd\3\2\2\2"+
		"#\u0107\3\2\2\2%\u010e\3\2\2\2\'\u0113\3\2\2\2)\u0117\3\2\2\2+\u011d\3"+
		"\2\2\2-\u0122\3\2\2\2/\u0128\3\2\2\2\61\u012e\3\2\2\2\63\u0130\3\2\2\2"+
		"\65\u0133\3\2\2\2\67\u0135\3\2\2\29\u0138\3\2\2\2;\u013a\3\2\2\2=\u013d"+
		"\3\2\2\2?\u013f\3\2\2\2A\u0142\3\2\2\2C\u0144\3\2\2\2E\u0146\3\2\2\2G"+
		"\u0148\3\2\2\2I\u014b\3\2\2\2K\u014e\3\2\2\2M\u0151\3\2\2\2O\u0154\3\2"+
		"\2\2Q\u0156\3\2\2\2S\u0158\3\2\2\2U\u015b\3\2\2\2W\u015e\3\2\2\2Y\u0162"+
		"\3\2\2\2[\u0165\3\2\2\2]\u0168\3\2\2\2_\u016c\3\2\2\2a\u0170\3\2\2\2c"+
		"\u0172\3\2\2\2e\u0174\3\2\2\2g\u0176\3\2\2\2i\u0178\3\2\2\2k\u017a\3\2"+
		"\2\2m\u017c\3\2\2\2o\u017e\3\2\2\2q\u0180\3\2\2\2s\u0182\3\2\2\2u\u018c"+
		"\3\2\2\2w\u018e\3\2\2\2y\u0197\3\2\2\2{\u01a3\3\2\2\2}\u01a5\3\2\2\2\177"+
		"\u01ad\3\2\2\2\u0081\u01b1\3\2\2\2\u0083\u01b7\3\2\2\2\u0085\u01c1\3\2"+
		"\2\2\u0087\u01c3\3\2\2\2\u0089\u01ca\3\2\2\2\u008b\u01cc\3\2\2\2\u008d"+
		"\u01d7\3\2\2\2\u008f\u01dd\3\2\2\2\u0091\u01e9\3\2\2\2\u0093\u01f8\3\2"+
		"\2\2\u0095\u0203\3\2\2\2\u0097\u0098\7,\2\2\u0098\u0099\7,\2\2\u0099\u009d"+
		"\3\2\2\2\u009a\u009c\13\2\2\2\u009b\u009a\3\2\2\2\u009c\u009f\3\2\2\2"+
		"\u009d\u009e\3\2\2\2\u009d\u009b\3\2\2\2\u009e\u00a0\3\2\2\2\u009f\u009d"+
		"\3\2\2\2\u00a0\u00a1\7,\2\2\u00a1\u00a2\7,\2\2\u00a2\u00a3\3\2\2\2\u00a3"+
		"\u00a4\b\2\2\2\u00a4\4\3\2\2\2\u00a5\u00a9\t\2\2\2\u00a6\u00a8\t\3\2\2"+
		"\u00a7\u00a6\3\2\2\2\u00a8\u00ab\3\2\2\2\u00a9\u00a7\3\2\2\2\u00a9\u00aa"+
		"\3\2\2\2\u00aa\6\3\2\2\2\u00ab\u00a9\3\2\2\2\u00ac\u00ad\7D\2\2\u00ad"+
		"\u00ae\7q\2\2\u00ae\u00af\7f\2\2\u00af\u00b0\7{\2\2\u00b0\b\3\2\2\2\u00b1"+
		"\u00b2\7D\2\2\u00b2\u00b3\7t\2\2\u00b3\u00b4\7g\2\2\u00b4\u00b5\7c\2\2"+
		"\u00b5\u00b6\7m\2\2\u00b6\n\3\2\2\2\u00b7\u00b8\7E\2\2\u00b8\u00b9\7q"+
		"\2\2\u00b9\u00ba\7p\2\2\u00ba\u00bb\7v\2\2\u00bb\u00bc\7k\2\2\u00bc\u00bd"+
		"\7p\2\2\u00bd\u00be\7w\2\2\u00be\u00bf\7g\2\2\u00bf\f\3\2\2\2\u00c0\u00c1"+
		"\7F\2\2\u00c1\u00c2\7q\2\2\u00c2\16\3\2\2\2\u00c3\u00c4\7G\2\2\u00c4\u00c5"+
		"\7n\2\2\u00c5\u00c6\7u\2\2\u00c6\u00c7\7g\2\2\u00c7\20\3\2\2\2\u00c8\u00c9"+
		"\7G\2\2\u00c9\u00ca\7n\2\2\u00ca\u00cb\7u\2\2\u00cb\u00cc\7g\2\2\u00cc"+
		"\u00cd\7K\2\2\u00cd\u00ce\7h\2\2\u00ce\22\3\2\2\2\u00cf\u00d0\7G\2\2\u00d0"+
		"\u00d1\7p\2\2\u00d1\u00d2\7f\2\2\u00d2\u00d3\7D\2\2\u00d3\u00d4\7q\2\2"+
		"\u00d4\u00d5\7f\2\2\u00d5\u00d6\7{\2\2\u00d6\24\3\2\2\2\u00d7\u00d8\7"+
		"G\2\2\u00d8\u00d9\7p\2\2\u00d9\u00da\7f\2\2\u00da\u00db\7K\2\2\u00db\u00dc"+
		"\7h\2\2\u00dc\26\3\2\2\2\u00dd\u00de\7G\2\2\u00de\u00df\7p\2\2\u00df\u00e0"+
		"\7f\2\2\u00e0\u00e1\7H\2\2\u00e1\u00e2\7q\2\2\u00e2\u00e3\7t\2\2\u00e3"+
		"\30\3\2\2\2\u00e4\u00e5\7G\2\2\u00e5\u00e6\7p\2\2\u00e6\u00e7\7f\2\2\u00e7"+
		"\u00e8\7Y\2\2\u00e8\u00e9\7j\2\2\u00e9\u00ea\7k\2\2\u00ea\u00eb\7n\2\2"+
		"\u00eb\u00ec\7g\2\2\u00ec\32\3\2\2\2\u00ed\u00ee\7H\2\2\u00ee\u00ef\7"+
		"q\2\2\u00ef\u00f0\7t\2\2\u00f0\34\3\2\2\2\u00f1\u00f2\7H\2\2\u00f2\u00f3"+
		"\7w\2\2\u00f3\u00f4\7p\2\2\u00f4\u00f5\7e\2\2\u00f5\u00f6\7v\2\2\u00f6"+
		"\u00f7\7k\2\2\u00f7\u00f8\7q\2\2\u00f8\u00f9\7p\2\2\u00f9\36\3\2\2\2\u00fa"+
		"\u00fb\7K\2\2\u00fb\u00fc\7h\2\2\u00fc \3\2\2\2\u00fd\u00fe\7R\2\2\u00fe"+
		"\u00ff\7c\2\2\u00ff\u0100\7t\2\2\u0100\u0101\7c\2\2\u0101\u0102\7o\2\2"+
		"\u0102\u0103\7g\2\2\u0103\u0104\7v\2\2\u0104\u0105\7g\2\2\u0105\u0106"+
		"\7t\2\2\u0106\"\3\2\2\2\u0107\u0108\7T\2\2\u0108\u0109\7g\2\2\u0109\u010a"+
		"\7v\2\2\u010a\u010b\7w\2\2\u010b\u010c\7t\2\2\u010c\u010d\7p\2\2\u010d"+
		"$\3\2\2\2\u010e\u010f\7V\2\2\u010f\u0110\7j\2\2\u0110\u0111\7g\2\2\u0111"+
		"\u0112\7p\2\2\u0112&\3\2\2\2\u0113\u0114\7X\2\2\u0114\u0115\7c\2\2\u0115"+
		"\u0116\7t\2\2\u0116(\3\2\2\2\u0117\u0118\7Y\2\2\u0118\u0119\7j\2\2\u0119"+
		"\u011a\7k\2\2\u011a\u011b\7n\2\2\u011b\u011c\7g\2\2\u011c*\3\2\2\2\u011d"+
		"\u011e\7V\2\2\u011e\u011f\7t\2\2\u011f\u0120\7w\2\2\u0120\u0121\7g\2\2"+
		"\u0121,\3\2\2\2\u0122\u0123\7H\2\2\u0123\u0124\7c\2\2\u0124\u0125\7n\2"+
		"\2\u0125\u0126\7u\2\2\u0126\u0127\7g\2\2\u0127.\3\2\2\2\u0128\u0129\7"+
		"G\2\2\u0129\u012a\7p\2\2\u012a\u012b\7f\2\2\u012b\u012c\7F\2\2\u012c\u012d"+
		"\7q\2\2\u012d\60\3\2\2\2\u012e\u012f\7-\2\2\u012f\62\3\2\2\2\u0130\u0131"+
		"\7-\2\2\u0131\u0132\7\60\2\2\u0132\64\3\2\2\2\u0133\u0134\7/\2\2\u0134"+
		"\66\3\2\2\2\u0135\u0136\7/\2\2\u0136\u0137\7\60\2\2\u01378\3\2\2\2\u0138"+
		"\u0139\7,\2\2\u0139:\3\2\2\2\u013a\u013b\7,\2\2\u013b\u013c\7\60\2\2\u013c"+
		"<\3\2\2\2\u013d\u013e\7^\2\2\u013e>\3\2\2\2\u013f\u0140\7^\2\2\u0140\u0141"+
		"\7\60\2\2\u0141@\3\2\2\2\u0142\u0143\7\'\2\2\u0143B\3\2\2\2\u0144\u0145"+
		"\7?\2\2\u0145D\3\2\2\2\u0146\u0147\7#\2\2\u0147F\3\2\2\2\u0148\u0149\7"+
		"(\2\2\u0149\u014a\7(\2\2\u014aH\3\2\2\2\u014b\u014c\7~\2\2\u014c\u014d"+
		"\7~\2\2\u014dJ\3\2\2\2\u014e\u014f\7?\2\2\u014f\u0150\7?\2\2\u0150L\3"+
		"\2\2\2\u0151\u0152\7#\2\2\u0152\u0153\7?\2\2\u0153N\3\2\2\2\u0154\u0155"+
		"\7>\2\2\u0155P\3\2\2\2\u0156\u0157\7@\2\2\u0157R\3\2\2\2\u0158\u0159\7"+
		">\2\2\u0159\u015a\7?\2\2\u015aT\3\2\2\2\u015b\u015c\7@\2\2\u015c\u015d"+
		"\7?\2\2\u015dV\3\2\2\2\u015e\u015f\7?\2\2\u015f\u0160\7\61\2\2\u0160\u0161"+
		"\7?\2\2\u0161X\3\2\2\2\u0162\u0163\7>\2\2\u0163\u0164\7\60\2\2\u0164Z"+
		"\3\2\2\2\u0165\u0166\7@\2\2\u0166\u0167\7\60\2\2\u0167\\\3\2\2\2\u0168"+
		"\u0169\7>\2\2\u0169\u016a\7?\2\2\u016a\u016b\7\60\2\2\u016b^\3\2\2\2\u016c"+
		"\u016d\7@\2\2\u016d\u016e\7?\2\2\u016e\u016f\7\60\2\2\u016f`\3\2\2\2\u0170"+
		"\u0171\7*\2\2\u0171b\3\2\2\2\u0172\u0173\7+\2\2\u0173d\3\2\2\2\u0174\u0175"+
		"\7]\2\2\u0175f\3\2\2\2\u0176\u0177\7_\2\2\u0177h\3\2\2\2\u0178\u0179\7"+
		"=\2\2\u0179j\3\2\2\2\u017a\u017b\7\60\2\2\u017bl\3\2\2\2\u017c\u017d\7"+
		".\2\2\u017dn\3\2\2\2\u017e\u017f\7<\2\2\u017fp\3\2\2\2\u0180\u0181\7}"+
		"\2\2\u0181r\3\2\2\2\u0182\u0183\7\177\2\2\u0183t\3\2\2\2\u0184\u018d\7"+
		"\62\2\2\u0185\u0189\t\4\2\2\u0186\u0188\t\5\2\2\u0187\u0186\3\2\2\2\u0188"+
		"\u018b\3\2\2\2\u0189\u0187\3\2\2\2\u0189\u018a\3\2\2\2\u018a\u018d\3\2"+
		"\2\2\u018b\u0189\3\2\2\2\u018c\u0184\3\2\2\2\u018c\u0185\3\2\2\2\u018d"+
		"v\3\2\2\2\u018e\u018f\7\62\2\2\u018f\u0190\t\6\2\2\u0190\u0194\t\7\2\2"+
		"\u0191\u0193\t\b\2\2\u0192\u0191\3\2\2\2\u0193\u0196\3\2\2\2\u0194\u0192"+
		"\3\2\2\2\u0194\u0195\3\2\2\2\u0195x\3\2\2\2\u0196\u0194\3\2\2\2\u0197"+
		"\u0198\7\62\2\2\u0198\u0199\t\t\2\2\u0199\u019d\t\n\2\2\u019a\u019c\t"+
		"\13\2\2\u019b\u019a\3\2\2\2\u019c\u019f\3\2\2\2\u019d\u019b\3\2\2\2\u019d"+
		"\u019e\3\2\2\2\u019ez\3\2\2\2\u019f\u019d\3\2\2\2\u01a0\u01a4\5u;\2\u01a1"+
		"\u01a4\5w<\2\u01a2\u01a4\5y=\2\u01a3\u01a0\3\2\2\2\u01a3\u01a1\3\2\2\2"+
		"\u01a3\u01a2\3\2\2\2\u01a4|\3\2\2\2\u01a5\u01a9\7\60\2\2\u01a6\u01a8\t"+
		"\5\2\2\u01a7\u01a6\3\2\2\2\u01a8\u01ab\3\2\2\2\u01a9\u01a7\3\2\2\2\u01a9"+
		"\u01aa\3\2\2\2\u01aa~\3\2\2\2\u01ab\u01a9\3\2\2\2\u01ac\u01ae\t\5\2\2"+
		"\u01ad\u01ac\3\2\2\2\u01ae\u01af\3\2\2\2\u01af\u01ad\3\2\2\2\u01af\u01b0"+
		"\3\2\2\2\u01b0\u0080\3\2\2\2\u01b1\u01b3\t\f\2\2\u01b2\u01b4\t\r\2\2\u01b3"+
		"\u01b2\3\2\2\2\u01b3\u01b4\3\2\2\2\u01b4\u01b5\3\2\2\2\u01b5\u01b6\5\177"+
		"@\2\u01b6\u0082\3\2\2\2\u01b7\u01bd\5\177@\2\u01b8\u01ba\5}?\2\u01b9\u01bb"+
		"\5\u0081A\2\u01ba\u01b9\3\2\2\2\u01ba\u01bb\3\2\2\2\u01bb\u01be\3\2\2"+
		"\2\u01bc\u01be\5\u0081A\2\u01bd\u01b8\3\2\2\2\u01bd\u01bc\3\2\2\2\u01be"+
		"\u0084\3\2\2\2\u01bf\u01c2\5+\26\2\u01c0\u01c2\5-\27\2\u01c1\u01bf\3\2"+
		"\2\2\u01c1\u01c0\3\2\2\2\u01c2\u0086\3\2\2\2\u01c3\u01c4\t\16\2\2\u01c4"+
		"\u0088\3\2\2\2\u01c5\u01c6\7)\2\2\u01c6\u01cb\7$\2\2\u01c7\u01c8\t\17"+
		"\2\2\u01c8\u01cb\5\u0087D\2\u01c9\u01cb\n\20\2\2\u01ca\u01c5\3\2\2\2\u01ca"+
		"\u01c7\3\2\2\2\u01ca\u01c9\3\2\2\2\u01cb\u008a\3\2\2\2\u01cc\u01d0\7$"+
		"\2\2\u01cd\u01cf\5\u0089E\2\u01ce\u01cd\3\2\2\2\u01cf\u01d2\3\2\2\2\u01d0"+
		"\u01ce\3\2\2\2\u01d0\u01d1\3\2\2\2\u01d1\u01d3\3\2\2\2\u01d2\u01d0\3\2"+
		"\2\2\u01d3\u01d4\7$\2\2\u01d4\u01d5\bF\3\2\u01d5\u008c\3\2\2\2\u01d6\u01d8"+
		"\t\21\2\2\u01d7\u01d6\3\2\2\2\u01d8\u01d9\3\2\2\2\u01d9\u01d7\3\2\2\2"+
		"\u01d9\u01da\3\2\2\2\u01da\u01db\3\2\2\2\u01db\u01dc\bG\2\2\u01dc\u008e"+
		"\3\2\2\2\u01dd\u01e1\7$\2\2\u01de\u01e0\5\u0089E\2\u01df\u01de\3\2\2\2"+
		"\u01e0\u01e3\3\2\2\2\u01e1\u01df\3\2\2\2\u01e1\u01e2\3\2\2\2\u01e2\u01e5"+
		"\3\2\2\2\u01e3\u01e1\3\2\2\2\u01e4\u01e6\t\22\2\2\u01e5\u01e4\3\2\2\2"+
		"\u01e6\u01e7\3\2\2\2\u01e7\u01e8\bH\4\2\u01e8\u0090\3\2\2\2\u01e9\u01ed"+
		"\7$\2\2\u01ea\u01ec\5\u0089E\2\u01eb\u01ea\3\2\2\2\u01ec\u01ef\3\2\2\2"+
		"\u01ed\u01eb\3\2\2\2\u01ed\u01ee\3\2\2\2\u01ee\u01f4\3\2\2\2\u01ef\u01ed"+
		"\3\2\2\2\u01f0\u01f1\t\17\2\2\u01f1\u01f5\n\16\2\2\u01f2\u01f3\t\23\2"+
		"\2\u01f3\u01f5\n\24\2\2\u01f4\u01f0\3\2\2\2\u01f4\u01f2\3\2\2\2\u01f5"+
		"\u01f6\3\2\2\2\u01f6\u01f7\bI\5\2\u01f7\u0092\3\2\2\2\u01f8\u01f9\7,\2"+
		"\2\u01f9\u01fa\7,\2\2\u01fa\u0200\3\2\2\2\u01fb\u01ff\n\25\2\2\u01fc\u01fd"+
		"\7,\2\2\u01fd\u01ff\n\25\2\2\u01fe\u01fb\3\2\2\2\u01fe\u01fc\3\2\2\2\u01ff"+
		"\u0202\3\2\2\2\u0200\u01fe\3\2\2\2\u0200\u0201\3\2\2\2\u0201\u0094\3\2"+
		"\2\2\u0202\u0200\3\2\2\2\u0203\u0213\5q9\2\u0204\u0214\5{>\2\u0205\u0214"+
		"\5\u0083B\2\u0206\u0214\5\u0085C\2\u0207\u0210\5\u008bF\2\u0208\u0209"+
		"\5m\67\2\u0209\u020a\5{>\2\u020a\u020f\3\2\2\2\u020b\u020f\5\u0083B\2"+
		"\u020c\u020f\5\u0085C\2\u020d\u020f\5\u008bF\2\u020e\u0208\3\2\2\2\u020e"+
		"\u020b\3\2\2\2\u020e\u020c\3\2\2\2\u020e\u020d\3\2\2\2\u020f\u0212\3\2"+
		"\2\2\u0210\u020e\3\2\2\2\u0210\u0211\3\2\2\2\u0211\u0214\3\2\2\2\u0212"+
		"\u0210\3\2\2\2\u0213\u0204\3\2\2\2\u0213\u0205\3\2\2\2\u0213\u0206\3\2"+
		"\2\2\u0213\u0207\3\2\2\2\u0213\u0214\3\2\2\2\u0214\u0215\3\2\2\2\u0215"+
		"\u0216\5s:\2\u0216\u0096\3\2\2\2\34\2\u009d\u00a9\u0189\u018c\u0194\u019d"+
		"\u01a3\u01a9\u01af\u01b3\u01ba\u01bd\u01c1\u01ca\u01d0\u01d9\u01e1\u01e5"+
		"\u01ed\u01f4\u01fe\u0200\u020e\u0210\u0213\6\b\2\2\3F\2\3H\3\3I\4";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}