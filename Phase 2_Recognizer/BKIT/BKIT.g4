/**
* Defined a grammar called MC
 */

 grammar BKIT;
 options{
     language=Java;
    //  language=Python3;
 }

 @lexer::header{

}

 @parser::header{

 }


program  : (var_declare SEMI)* func_declare* EOF ; //start symbol
//2.1 Variable declaration
var_declare: VAR COLON id_init (COMMA id_init)*;
id_init: init_variable (ASGN all_literal)?;
init_variable: ID (SBRACKET_L (INT_LIT) SBRACKET_R)*;

//2.2 Function declaration part
func_declare: FUNC COLON ID func_para? func_body;
func_para: PARA COLON init_variable (COMMA init_variable)*;
func_body: BODY COLON statement_list ENDBODY DOT;



all_literal: single_lit | array_lit;
single_lit: INT_LIT | FLOAT_LIT | BOOL_LIT |STRING_LIT;
//4.5 Array type
array_lit: CBRACKET_L  (all_literal (COMMA all_literal)*)? CBRACKET_R  ;// the ? is for empty array


//6.3 Relational operators
expression: exp2 (EQUAL | NEQUAL | LESS | GREATER | SEQUAL | LEQUAL | NEQUALF | LESSF | GREATERF | SEQUALF | LEQUALF) exp2
		  | exp2;
exp2: RBRACKET_L expression RBRACKET_R
	| funcCall
	| index_exp
	| <assoc=right> (SUB|SUBF) exp2
	| <assoc=right> (NOT) exp2
	| exp2(MUL|MULF|DIV|DIVF|MOD)exp2
	| exp2(ADD|ADDF|SUB|SUBF)exp2
	| exp2(AND|OR)exp2
	| ID
	| all_literal;

//6.4 Index operators
index_exp: (ID | funcCall) (SBRACKET_L expression SBRACKET_R)+ ;

//7 Statements
statement_list: (var_declare SEMI)* statement*;
statement: (assignment | BREAK | CONT | funcCall | ret_stmt) SEMI
		 | (if_stmt | for_stmt | while_stmt | do_stmt); //these stmt don't end with semicolon

assignment: (ID | index_exp) ASGN expression;

if_stmt: IF expression THEN statement_list (ELSEIF expression THEN statement_list)* (ELSE statement_list)? ENDIF DOT;

for_stmt: FOR RBRACKET_L ID ASGN expression COMMA expression COMMA expression RBRACKET_R DO statement_list ENDFOR DOT;

while_stmt: WHILE expression DO statement_list ENDWHILE DOT;

do_stmt: DO statement_list WHILE expression ENDDO DOT;

funcCall: ID RBRACKET_L (expression(COMMA expression)*)? RBRACKET_R;

ret_stmt: RETURN expression?;

//3. Lexical Structure

//3.2  Block Comments
COMMENT: '**' .*? '**' -> skip ;
//3.3.1 Identifiers
ID: [a-z][a-zA-Z0-9_]* ;

//3.3.2 Keywords
BODY: 'Body' ;
BREAK: 'Break' ;
CONT: 'Continue' ;
DO: 'Do' ;
ELSE: 'Else' ;
ELSEIF: 'ElseIf' ;
ENDBODY: 'EndBody' ;
ENDIF: 'EndIf' ;
ENDFOR: 'EndFor' ;
ENDWHILE: 'EndWhile' ;
FOR: 'For' ;
FUNC: 'Function' ;
IF: 'If' ;
PARA: 'Parameter' ;
RETURN: 'Return' ;
THEN: 'Then' ;
VAR: 'Var' ;
WHILE: 'While' ;
fragment TRUE: 'True' ;
fragment FALSE: 'False' ;
ENDDO: 'EndDo' ;

//3.3.3 Operators
ADD: '+' ;
ADDF: '+.' ;
SUB: '-' ;
SUBF: '-.' ;
MUL: '*' ;
MULF: '*.' ;
DIV: '\\' ;
DIVF: '\\.' ;
MOD: '%' ;
ASGN: '=';


NOT: '!' ;
AND: '&&' ;
OR: '||' ;

EQUAL: '==' ;
NEQUAL: '!=' ;
LESS: '<' ;
GREATER: '>' ;
SEQUAL: '<=' ;
LEQUAL: '>=' ;

NEQUALF: '=/=' ;
LESSF: '<.' ;
GREATERF: '>.' ;
SEQUALF: '<=.' ;
LEQUALF: '>=.' ;

//3.3.4 Seperators
RBRACKET_L: '(' ;
RBRACKET_R: ')' ;
SBRACKET_L: '[' ;
SBRACKET_R: ']' ;
SEMI: ';' ;
DOT: '.' ;
COMMA: ',' ;
COLON: ':' ;
CBRACKET_L: '{' ;
CBRACKET_R: '}' ;

fragment DEC_LIT: '0' | [1-9][0-9]*;
fragment HEX_LIT: '0'[xX][1-9A-F][0-9A-F]* ;
fragment OCT_LIT: '0'[oO][1-7][0-7]* ;
INT_LIT: DEC_LIT | HEX_LIT | OCT_LIT;

fragment DecimalPart: ('.'[0-9]*) ;
fragment Digits: [0-9]+ ;
fragment ExponentPart: [eE] [+-]? Digits ;
FLOAT_LIT: Digits (DecimalPart ExponentPart? | ExponentPart) ;

BOOL_LIT:(TRUE|FALSE) ;

fragment LegalEscape: [bfrnt'\\] ;
fragment StringElement:  '\'"' | [\\] LegalEscape | ~['\\\n\r"] ;
STRING_LIT:
	'"' StringElement* '"'
     {setText(getText().substring(1, getText().length()-1));};

//Whitespace
WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines


//Errors Tokens
UNCLOSE_STRING:
	'"' StringElement* ('\r'|'\n'|EOF)
	     {setText(getText().substring(1, getText().length()-1));};

ILLEGAL_ESCAPE:
	'"'  StringElement* ( [\\]~[bfrnt'\\] | [']~["])
	     {setText(getText().substring(1, getText().length()-1));};

UNTERMINATED_COMMENT: '**' ( (~'*') | '*'(~'*') )*  ;


ARRAY_LIT: CBRACKET_L  (INT_LIT | FLOAT_LIT | BOOL_LIT |STRING_LIT (COMMA INT_LIT | FLOAT_LIT | BOOL_LIT |STRING_LIT)*)? CBRACKET_R  ;// the ? is for empty array