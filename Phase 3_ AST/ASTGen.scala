import java.io.{File, PrintWriter}
import java.lang.RuntimeException

import org.antlr.v4.runtime.ANTLRFileStream
import org.antlr.v4.runtime.CommonTokenStream

trait AST
//case class Program(varDecl:List[VarDecl], funcDecl: List[FuncDecl]) extends AST
case class Program(decl: List[Decl]) extends AST
trait Stmt extends AST
case class AssignStmt (lhs: LHS, rhs: Exp) extends Stmt
case class IfStmt(ifList: List[(Exp,StmtList)], elseList: StmtList) extends Stmt
case class ForStmt(idx: Id, exp1: Exp, exp2: Exp, exp3: Exp,stmtList: StmtList ) extends Stmt
case class BreakStmt() extends Stmt
case class Continue() extends Stmt
case class ReturnStmt(exp: Exp) extends Stmt
case class DoWhileStmt(exp: Exp, stmtList: StmtList) extends Stmt
case class WhileStmt(exp: Exp, stmtList: StmtList) extends Stmt
case class FuncCallStmt(method:Id, para: List[Exp]) extends Stmt
trait Exp extends AST
case class BinOp(op:String,e1:Exp,e2:Exp) extends Exp
case class UnOp(op:String,e1:Exp) extends Exp
case class FuncCallExp(name: Id, para: List[Exp]) extends Exp
trait Decl extends AST
//case class VarDecl(variable: Id, varDimen: List[Int], varInit: Literal) extends Decl
//case class IdInit(initVar: String, lit: List[Literal]) extends Decl
case class VarDecl(varDecl: List[IdInit]) extends Decl
case class IdInit(variable: Id, varDimen: List[Int], varInit: Literal) extends Decl
case class FuncDecl(name: Id, param: FuncParam, body: FuncBody) extends Decl
case class FuncBody(stmtList: StmtList) extends Decl
case class FuncParam(param: List[Decl]) extends Decl
case class StmtList(varDecl: List[VarDecl], stmtList:List[Stmt]) extends Decl
trait Literal extends Exp
case class Intlit(lit:Int) extends Literal
case class Floatlit(lit:Float) extends Literal
case class Stringlit(lit:String) extends Literal
case class Boollit(lit:Boolean) extends Literal
case class Arraylit(lit:List[Literal]) extends Literal
trait LHS extends Exp
case class Id(name: String) extends LHS
case class ArrayCell(arr: Exp, idx: List[Exp]) extends LHS //rename
case class IdVariable(id: String, dimen: List[Int]) extends LHS


class ASTGen extends BKITBaseVisitor[AST] {
  //program  : (var_declare SEMI)* func_declare* EOF ; //start symbol
  override def visitProgram(ctx: BKITParser.ProgramContext) = {
    println("Visit Prog")
    var res = List[Decl]()
    var len = ctx.var_declare().size()
    println("ABC " + len)
    var res2 = List[FuncDecl]()
    var len2 = ctx.func_declare().size()

    if (len > 0) {
      res = ctx.var_declare(0).accept(this).asInstanceOf[Decl] :: res
      for (i <- 1 to ctx.var_declare().size() - 1) {
        res = ctx.var_declare(i).accept(this).asInstanceOf[Decl] :: res
        println("VarDecl: " + res.length)
      }
    }
    if (len2 > 0) {
      println("ABC " + len2)
      res = ctx.func_declare(0).accept(this).asInstanceOf[Decl] :: res
      for (i <- 1 to ctx.func_declare().size() - 1) {
        res = ctx.func_declare(i).accept(this).asInstanceOf[Decl] :: res
        println("EXP: " + res.length)
      }

    }

    Program(res)

  }

  //  # all_literal: basic_literal | array_lit;
  override def visitAll_literal(ctx: BKITParser.All_literalContext) = {
    println("Visit All_literal")
    ctx.getChild(0).accept(this)
  }

  //single_lit: INT_LIT | FLOAT_LIT | BOOL_LIT |STRING_LIT;
  override def visitSingle_lit(ctx: BKITParser.Single_litContext) = {
    println("Visit Single_lit")
    if (ctx.INT_LIT() != null) {
      println("Visit INT_LIT: " + ctx.INT_LIT().getText)
      Intlit(ctx.INT_LIT().getText().toInt)
    }
    else if (ctx.FLOAT_LIT() != null) {
      println("Visit FLOAT: " + ctx.FLOAT_LIT().getText)
      Floatlit(ctx.FLOAT_LIT().getText().toFloat)
    }
    else if (ctx.BOOL_LIT() != null) {
      println("Visit BOOL: " + ctx.BOOL_LIT().getText)
      Boollit(ctx.BOOL_LIT().getText().toBoolean)
    }
    else Stringlit(ctx.STRING_LIT().getText)
  }

  // array_lit: CBRACKET_L  (all_literal (COMMA all_literal)*)? CBRACKET_R  ;// the ? is for empty array
  override def visitArray_lit(ctx: BKITParser.Array_litContext) = {
    println("Visit Array_lit")
    var res = List[Literal]()
    if (ctx.all_literal().size() > 0)
      for (i <- 0 to ctx.all_literal().size() - 1) {
        res = ctx.all_literal(i).accept(this).asInstanceOf[Literal] :: res
      }
    Arraylit(res)
  }

  // func_declare: FUNC COLON ID func_para? func_body;
  override def visitFunc_declare(ctx: BKITParser.Func_declareContext) = {
    println("Visit Func_declare")



    /*  if (ctx.func_para().size() > 0)
        for (i <- 0 to all_literal().size() - 1) {
          param = ctx.func_para(i).accept(this).asInstanceOf[Literal] :: res
        }*/
    var body = ctx.func_body().accept(this).asInstanceOf[FuncBody]

    if (ctx.func_para() != null) {
      var param = ctx.func_para().accept(this).asInstanceOf[FuncParam]
      FuncDecl(Id(ctx.ID().getText), param, body)
    }
    else
    // var param =  ctx.func_para().accept(this).asInstanceOf[List[VarDecl]]

      FuncDecl(Id(ctx.ID().getText), null, body)
  }


  // func_body: BODY statement_list ENDBODY DOT;
  override def visitFunc_body(ctx: BKITParser.Func_bodyContext) = {
    println("Visit Func_body")
    FuncBody(ctx.statement_list().accept(this).asInstanceOf[StmtList])
  }

  //func_para: PARA COLON init_variable (COMMA init_variable)*;
  override def visitFunc_para(ctx: BKITParser.Func_paraContext) = {
    var res = List[Decl]()
    res = ctx.id_init(0).accept(this).asInstanceOf[Decl] :: res
    if (ctx.id_init().size() > 1)
      for (i <- 1 to ctx.id_init().size() - 1) {
        res = ctx.id_init(i).accept(this).asInstanceOf[Decl] :: res
      }
    FuncParam(res)

  }

  //statement_list: (var_declare SEMI)* statement*;
  override def visitStatement_list(ctx: BKITParser.Statement_listContext) = {
    println("Visit Statement_list")
    var varDeclList = List[VarDecl]()
    if (ctx.var_declare().size() > 0)
      for (i <- 0 to ctx.var_declare().size() - 1) {
        varDeclList = ctx.var_declare(i).accept(this).asInstanceOf[VarDecl] :: varDeclList
      }
    var stmtlList = List[Stmt]()
    if (ctx.statement().size() > 0) {
      println("check Statement_list")
      for (i <- 0 to ctx.statement().size() - 1) {
        stmtlList = ctx.statement(i).accept(this).asInstanceOf[Stmt] :: stmtlList
      }
    }
    StmtList(varDeclList, stmtlList)
  }

  // statement: (assignment | BREAK | CONT | funcCall | ret_stmt) SEMI
  // | (if_stmt | for_stmt | while_stmt | do_stmt);
  override def visitStatement(ctx: BKITParser.StatementContext) = {
    println("Visit Statement")
    if (ctx.BREAK() != null) BreakStmt()
    else if (ctx.CONT() != null) Continue()
   else {
     println(ctx.getChild(0).getText)
     ctx.getChild(0).accept(this)
   }

  }

  //var_declare: VAR COLON id_init (COMMA id_init)*;
  override def visitVar_declare(ctx: BKITParser.Var_declareContext) = {
    println("Visit Var declare")
    var varDeclList = List[IdInit]()
    varDeclList = ctx.id_init(0).accept(this).asInstanceOf[IdInit] :: varDeclList
    if (ctx.id_init().size() > 1)
      for (i <- 1 to ctx.id_init().size() - 1) {
        varDeclList = ctx.id_init(i).accept(this).asInstanceOf[IdInit] :: varDeclList
      }
    /*  var res = List(ctx.id_init(0).accept(this))
      if (ctx.id_init.size() > 1)
        for (i <- 1 to ctx.id_init.size() - 1) {
          res = ctx.id_init(i).accept(this) :: res
        }*/
    VarDecl(varDeclList)
  }

  //id_init: init_variable (ASGN all_literal)?;
  override def visitId_init(ctx: BKITParser.Id_initContext) = {
    println("Visit Id_init")
    var res = ctx.init_variable().accept(this).asInstanceOf[IdVariable]
    if (ctx.all_literal() != null) IdInit(Id(res.id), res.dimen, ctx.all_literal().accept(this).asInstanceOf[Literal])
    else IdInit(Id(res.id), res.dimen, null)
  }

  // init_variable: ID (SBRACKET_L (INT_LIT) SBRACKET_R)*;
  override def visitInit_variable(ctx: BKITParser.Init_variableContext) = {
    println("Visit Id_variable")
    var dimentions = List[Int]()
    var id = ctx.getChild(0).getText()
    if (ctx.SBRACKET_L().size() > 0) {
      for (i <- 0 to ctx.SBRACKET_L().size() - 1) {
        println(ctx.getChild(i * 3 + 2).getText())
        dimentions = ctx.getChild(i * 3 + 2).getText().toInt :: dimentions
      }

    }
    IdVariable(id, dimentions)
  }

  // assignment: (ID | index_exp) ASGN expression;
  override def visitAssignment(ctx: BKITParser.AssignmentContext) = {
    println("Visit Assignment")
    if (ctx.ID() != null) AssignStmt(Id(ctx.ID().getText), ctx.expression().accept(this).asInstanceOf[Exp])
    else AssignStmt(ctx.index_exp().accept(this).asInstanceOf[LHS], ctx.expression().accept(this).asInstanceOf[Exp])
  }

  //case class IfStmt(ifExp: Exp, ifStmt: List[StmtList])
  // if_stmt: IF expression THEN statement_list (ELSEIF expression THEN statement_list)* (ELSE statement_list)? ENDIF DOT;
  override def visitIf_stmt(ctx: BKITParser.If_stmtContext) = {
    println("Visit If_stmt")
    var ifList: List[(Exp, StmtList)] = List()
    var elseList = ctx.statement_list(ctx.statement_list.size() - 1).accept(this).asInstanceOf[StmtList]
    var sizeIf = ctx.THEN().size()
    var sizeElse = ctx.statement_list.size() - ctx.expression().size()
    ifList = List((ctx.expression(0).accept(this).asInstanceOf[Exp], ctx.statement_list(0).accept(this).asInstanceOf[StmtList]))
    if (sizeIf > 1)
      for (i <- 1 to sizeIf - 1) {
        ifList = (ctx.expression(i).accept(this).asInstanceOf[Exp], ctx.statement_list(i).accept(this).asInstanceOf[StmtList]) :: ifList
      }
    if (sizeElse == 0) {
      elseList = null
    }

    IfStmt(ifList, elseList)
  }

  //FOR RBRACKET_L ID ASGN expression COMMA expression COMMA expression RBRACKET_R DO statement_list ENDFOR DOT;
  override def visitFor_stmt(ctx: BKITParser.For_stmtContext) = {
    println("Visit For")
    ForStmt(Id(ctx.ID().getText()), ctx.expression(0).accept(this).asInstanceOf[Exp], ctx.expression(1).accept(this).asInstanceOf[Exp]
      , ctx.expression(2).accept(this).asInstanceOf[Exp], ctx.statement_list().accept(this).asInstanceOf[StmtList])

  }

  //while_stmt: WHILE expression DO statement_list ENDWHILE DOT;
  override def visitWhile_stmt(ctx: BKITParser.While_stmtContext) = {
    println("Visit While")
    WhileStmt(ctx.expression().accept(this).asInstanceOf[Exp], ctx.statement_list().accept(this).asInstanceOf[StmtList])

  }

  //dowhile_stmt: DO statement_list WHILE expression ENDDO DOT;
  override def visitDowhile_stmt(ctx: BKITParser.Dowhile_stmtContext) = {
    println("Visit Do While")
    DoWhileStmt(ctx.expression().accept(this).asInstanceOf[Exp], ctx.statement_list().accept(this).asInstanceOf[StmtList])

  }

  //  funcCall: ID RBRACKET_L (expression(COMMA expression)*)? RBRACKET_R;
  override def visitFuncCall(ctx: BKITParser.FuncCallContext) = {
    println("Visit FuncCall")
    var res = List[Exp]()
    if (ctx.expression().size() > 0)
      for (i <- 0 to ctx.expression().size() - 1) {
        res = ctx.expression(i).accept(this).asInstanceOf[Exp] :: res
      }
    FuncCallStmt(Id(ctx.ID().getText), res)


  }
  //ret_stmt: RETURN expression?;
  override def visitRet_stmt(ctx: BKITParser.Ret_stmtContext) = {
    println("Visit Return")

    if (ctx.expression() != null)
      ReturnStmt(ctx.expression().accept(this).asInstanceOf[Exp])
    else
      ReturnStmt(null)
  }

  //expression: exp2 (EQUAL | NEQUAL | LESS | GREATER | SEQUAL | LEQUAL | NEQUALF | LESSF | GREATERF | SEQUALF | LEQUALF) exp2
  //		  | exp2;
  override def visitExpression(ctx: BKITParser.ExpressionContext) = {
    println("Visit Expression")

    if (ctx.exp2().size() == 2)
      BinOp(ctx.getChild(1).getText(), ctx.exp2(0).accept(this).asInstanceOf[Exp], ctx.exp2(1).accept(this).asInstanceOf[Exp])
    else
      ctx.exp2(0).accept(this).asInstanceOf[Exp]

  }

  //    exp2: RBRACKET_L expression RBRACKET_R //not sure
  //	      | funcCall
  //	      | index_exp
  //	      | <assoc=right> (SUB|SUBF) exp2
  //	      | <assoc=right> (NOT) exp2
  //	      | exp2(MUL|MULF|DIV|DIVF|MOD)exp2
  //	      | exp2(ADD|ADDF|SUB|SUBF)exp2
  //	      | exp2(AND|OR)exp2
  //	      | ID
  //	      |  all_literal;
  //Can't resolve Return 3+2; => need to be enhance
  override def visitExp2(ctx: BKITParser.Exp2Context) = {
    println("Visit Exp2")




    if (ctx.RBRACKET_L() != null) ctx.expression().accept(this).asInstanceOf[Exp]
    else if (ctx.funcCall() != null) {
      var res = ctx.funcCall().accept(this).asInstanceOf[FuncCallStmt]
      FuncCallExp(res.method, res.para)
    }
   else if (ctx.index_exp() != null) ctx.index_exp().accept(this).asInstanceOf[ArrayCell]
    else if (ctx.ID()!=null)  Id(ctx.ID().getText())
    else if (ctx.all_literal() != null) ctx.all_literal().accept(this).asInstanceOf[Literal]


   else if (ctx.exp2().size == 1) UnOp(ctx.getChild(0).getText(), ctx.exp2(0).accept(this).asInstanceOf[Exp])



   else if (ctx.exp2().size == 2) BinOp(ctx.getChild(1).getText(), ctx.exp2(0).accept(this).asInstanceOf[Exp], ctx.exp2(1).accept(this).asInstanceOf[Exp])

   else if (ctx.getChild(0).accept(this).isInstanceOf[FuncCallStmt]) {
      var   res = ctx.getChild(0).accept(this).asInstanceOf[FuncCallStmt]
      FuncCallExp(res.method, res.para)
    }

    else {
      println("BHHH" + ctx.getChild(0).accept(this).asInstanceOf[ArrayCell])
      Id(ctx.getChild(0).toString)
    }

  }

  override def visitIndex_exp(ctx: BKITParser.Index_expContext) = {
    println("Visit Index_exp")
    var expList = List[Exp]()
    if (ctx.expression().size() > 0)
      for (i <- 0 to ctx.expression.size() - 1) {
        println(ctx.expression(i).getText)
        expList = ctx.expression(i).accept(this).asInstanceOf[Exp] :: expList
      }
    if (ctx.getChild(0).accept(this).isInstanceOf[FuncCallStmt]) {

      var res = ctx.funcCall().accept(this).asInstanceOf[FuncCallStmt]
      var funcCallExp = FuncCallExp(res.method, res.para)
      ArrayCell(funcCallExp, expList)

    }
    else ArrayCell(Id(ctx.ID().getText()), expList)
  }

}

object ASTGen extends App {
    val source = new ANTLRFileStream("test.txt")
    val lexer = new BKITLexer(source)
    val tokens = new CommonTokenStream(lexer)
    val parser = new BKITParser(tokens)
    val bkit_parsetree = parser.program()
    val astgen = new ASTGen()
    val bkit_ast = bkit_parsetree.accept(astgen)
    println(bkit_ast)

}
/*10:06 AM
Qui Trình:
antlr4  BKIT.g4
antlr4 -visitor BKIT.g4
javac BKIT*.java

grun BKIT program -tokens

scalac ASTGen.scala --> sinh ra file .class
scala ASTGen --> Chạy chương trình*/

