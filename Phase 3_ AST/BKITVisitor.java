// Generated from BKIT.g4 by ANTLR 4.9.2


import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link BKITParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface BKITVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link BKITParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(BKITParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKITParser#var_declare}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_declare(BKITParser.Var_declareContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKITParser#id_init}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitId_init(BKITParser.Id_initContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKITParser#init_variable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInit_variable(BKITParser.Init_variableContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKITParser#func_declare}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunc_declare(BKITParser.Func_declareContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKITParser#func_para}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunc_para(BKITParser.Func_paraContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKITParser#func_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunc_body(BKITParser.Func_bodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKITParser#all_literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAll_literal(BKITParser.All_literalContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKITParser#single_lit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSingle_lit(BKITParser.Single_litContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKITParser#array_lit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArray_lit(BKITParser.Array_litContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKITParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(BKITParser.ExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKITParser#exp2}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExp2(BKITParser.Exp2Context ctx);
	/**
	 * Visit a parse tree produced by {@link BKITParser#index_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndex_exp(BKITParser.Index_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKITParser#statement_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement_list(BKITParser.Statement_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKITParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(BKITParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKITParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment(BKITParser.AssignmentContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKITParser#if_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_stmt(BKITParser.If_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKITParser#for_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFor_stmt(BKITParser.For_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKITParser#while_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhile_stmt(BKITParser.While_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKITParser#dowhile_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDowhile_stmt(BKITParser.Dowhile_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKITParser#funcCall}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncCall(BKITParser.FuncCallContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKITParser#ret_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRet_stmt(BKITParser.Ret_stmtContext ctx);
}