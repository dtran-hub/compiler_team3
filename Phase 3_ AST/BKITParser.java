// Generated from BKIT.g4 by ANTLR 4.9.2


import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BKITParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.9.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		COMMENT=1, ID=2, BODY=3, BREAK=4, CONT=5, DO=6, ELSE=7, ELSEIF=8, ENDBODY=9, 
		ENDIF=10, ENDFOR=11, ENDWHILE=12, FOR=13, FUNC=14, IF=15, PARA=16, RETURN=17, 
		THEN=18, VAR=19, WHILE=20, ENDDO=21, ADD=22, ADDF=23, SUB=24, SUBF=25, 
		MUL=26, MULF=27, DIV=28, DIVF=29, MOD=30, ASGN=31, NOT=32, AND=33, OR=34, 
		EQUAL=35, NEQUAL=36, LESS=37, GREATER=38, SEQUAL=39, LEQUAL=40, NEQUALF=41, 
		LESSF=42, GREATERF=43, SEQUALF=44, LEQUALF=45, RBRACKET_L=46, RBRACKET_R=47, 
		SBRACKET_L=48, SBRACKET_R=49, SEMI=50, DOT=51, COMMA=52, COLON=53, CBRACKET_L=54, 
		CBRACKET_R=55, INT_LIT=56, FLOAT_LIT=57, BOOL_LIT=58, STRING_LIT=59, WS=60, 
		UNCLOSE_STRING=61, ILLEGAL_ESCAPE=62, UNTERMINATE_COMMENT=63, ARRAY_LIT=64, 
		ERROR_CHAR=65;
	public static final int
		RULE_program = 0, RULE_var_declare = 1, RULE_id_init = 2, RULE_init_variable = 3, 
		RULE_func_declare = 4, RULE_func_para = 5, RULE_func_body = 6, RULE_all_literal = 7, 
		RULE_single_lit = 8, RULE_array_lit = 9, RULE_expression = 10, RULE_exp2 = 11, 
		RULE_index_exp = 12, RULE_statement_list = 13, RULE_statement = 14, RULE_assignment = 15, 
		RULE_if_stmt = 16, RULE_for_stmt = 17, RULE_while_stmt = 18, RULE_dowhile_stmt = 19, 
		RULE_funcCall = 20, RULE_ret_stmt = 21;
	private static String[] makeRuleNames() {
		return new String[] {
			"program", "var_declare", "id_init", "init_variable", "func_declare", 
			"func_para", "func_body", "all_literal", "single_lit", "array_lit", "expression", 
			"exp2", "index_exp", "statement_list", "statement", "assignment", "if_stmt", 
			"for_stmt", "while_stmt", "dowhile_stmt", "funcCall", "ret_stmt"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, "'Body'", "'Break'", "'Continue'", "'Do'", "'Else'", 
			"'ElseIf'", "'EndBody'", "'EndIf'", "'EndFor'", "'EndWhile'", "'For'", 
			"'Function'", "'If'", "'Parameter'", "'Return'", "'Then'", "'Var'", "'While'", 
			"'EndDo'", "'+'", "'+.'", "'-'", "'-.'", "'*'", "'*.'", "'\\'", "'\\.'", 
			"'%'", "'='", "'!'", "'&&'", "'||'", "'=='", "'!='", "'<'", "'>'", "'<='", 
			"'>='", "'=/='", "'<.'", "'>.'", "'<=.'", "'>=.'", "'('", "')'", "'['", 
			"']'", "';'", "'.'", "','", "':'", "'{'", "'}'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "COMMENT", "ID", "BODY", "BREAK", "CONT", "DO", "ELSE", "ELSEIF", 
			"ENDBODY", "ENDIF", "ENDFOR", "ENDWHILE", "FOR", "FUNC", "IF", "PARA", 
			"RETURN", "THEN", "VAR", "WHILE", "ENDDO", "ADD", "ADDF", "SUB", "SUBF", 
			"MUL", "MULF", "DIV", "DIVF", "MOD", "ASGN", "NOT", "AND", "OR", "EQUAL", 
			"NEQUAL", "LESS", "GREATER", "SEQUAL", "LEQUAL", "NEQUALF", "LESSF", 
			"GREATERF", "SEQUALF", "LEQUALF", "RBRACKET_L", "RBRACKET_R", "SBRACKET_L", 
			"SBRACKET_R", "SEMI", "DOT", "COMMA", "COLON", "CBRACKET_L", "CBRACKET_R", 
			"INT_LIT", "FLOAT_LIT", "BOOL_LIT", "STRING_LIT", "WS", "UNCLOSE_STRING", 
			"ILLEGAL_ESCAPE", "UNTERMINATE_COMMENT", "ARRAY_LIT", "ERROR_CHAR"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "BKIT.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public BKITParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ProgramContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(BKITParser.EOF, 0); }
		public List<Var_declareContext> var_declare() {
			return getRuleContexts(Var_declareContext.class);
		}
		public Var_declareContext var_declare(int i) {
			return getRuleContext(Var_declareContext.class,i);
		}
		public List<TerminalNode> SEMI() { return getTokens(BKITParser.SEMI); }
		public TerminalNode SEMI(int i) {
			return getToken(BKITParser.SEMI, i);
		}
		public List<Func_declareContext> func_declare() {
			return getRuleContexts(Func_declareContext.class);
		}
		public Func_declareContext func_declare(int i) {
			return getRuleContext(Func_declareContext.class,i);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).exitProgram(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKITVisitor ) return ((BKITVisitor<? extends T>)visitor).visitProgram(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(49);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==VAR) {
				{
				{
				setState(44);
				var_declare();
				setState(45);
				match(SEMI);
				}
				}
				setState(51);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(55);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==FUNC) {
				{
				{
				setState(52);
				func_declare();
				}
				}
				setState(57);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(58);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Var_declareContext extends ParserRuleContext {
		public TerminalNode VAR() { return getToken(BKITParser.VAR, 0); }
		public TerminalNode COLON() { return getToken(BKITParser.COLON, 0); }
		public List<Id_initContext> id_init() {
			return getRuleContexts(Id_initContext.class);
		}
		public Id_initContext id_init(int i) {
			return getRuleContext(Id_initContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(BKITParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(BKITParser.COMMA, i);
		}
		public Var_declareContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_var_declare; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).enterVar_declare(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).exitVar_declare(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKITVisitor ) return ((BKITVisitor<? extends T>)visitor).visitVar_declare(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Var_declareContext var_declare() throws RecognitionException {
		Var_declareContext _localctx = new Var_declareContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_var_declare);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(60);
			match(VAR);
			setState(61);
			match(COLON);
			setState(62);
			id_init();
			setState(67);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(63);
				match(COMMA);
				setState(64);
				id_init();
				}
				}
				setState(69);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Id_initContext extends ParserRuleContext {
		public Init_variableContext init_variable() {
			return getRuleContext(Init_variableContext.class,0);
		}
		public TerminalNode ASGN() { return getToken(BKITParser.ASGN, 0); }
		public All_literalContext all_literal() {
			return getRuleContext(All_literalContext.class,0);
		}
		public Id_initContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_id_init; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).enterId_init(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).exitId_init(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKITVisitor ) return ((BKITVisitor<? extends T>)visitor).visitId_init(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Id_initContext id_init() throws RecognitionException {
		Id_initContext _localctx = new Id_initContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_id_init);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(70);
			init_variable();
			setState(73);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ASGN) {
				{
				setState(71);
				match(ASGN);
				setState(72);
				all_literal();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Init_variableContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(BKITParser.ID, 0); }
		public List<TerminalNode> SBRACKET_L() { return getTokens(BKITParser.SBRACKET_L); }
		public TerminalNode SBRACKET_L(int i) {
			return getToken(BKITParser.SBRACKET_L, i);
		}
		public List<TerminalNode> SBRACKET_R() { return getTokens(BKITParser.SBRACKET_R); }
		public TerminalNode SBRACKET_R(int i) {
			return getToken(BKITParser.SBRACKET_R, i);
		}
		public List<TerminalNode> INT_LIT() { return getTokens(BKITParser.INT_LIT); }
		public TerminalNode INT_LIT(int i) {
			return getToken(BKITParser.INT_LIT, i);
		}
		public Init_variableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_init_variable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).enterInit_variable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).exitInit_variable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKITVisitor ) return ((BKITVisitor<? extends T>)visitor).visitInit_variable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Init_variableContext init_variable() throws RecognitionException {
		Init_variableContext _localctx = new Init_variableContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_init_variable);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(75);
			match(ID);
			setState(81);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SBRACKET_L) {
				{
				{
				setState(76);
				match(SBRACKET_L);
				{
				setState(77);
				match(INT_LIT);
				}
				setState(78);
				match(SBRACKET_R);
				}
				}
				setState(83);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Func_declareContext extends ParserRuleContext {
		public TerminalNode FUNC() { return getToken(BKITParser.FUNC, 0); }
		public TerminalNode COLON() { return getToken(BKITParser.COLON, 0); }
		public TerminalNode ID() { return getToken(BKITParser.ID, 0); }
		public Func_bodyContext func_body() {
			return getRuleContext(Func_bodyContext.class,0);
		}
		public Func_paraContext func_para() {
			return getRuleContext(Func_paraContext.class,0);
		}
		public Func_declareContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_func_declare; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).enterFunc_declare(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).exitFunc_declare(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKITVisitor ) return ((BKITVisitor<? extends T>)visitor).visitFunc_declare(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Func_declareContext func_declare() throws RecognitionException {
		Func_declareContext _localctx = new Func_declareContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_func_declare);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(84);
			match(FUNC);
			setState(85);
			match(COLON);
			setState(86);
			match(ID);
			setState(88);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==PARA) {
				{
				setState(87);
				func_para();
				}
			}

			setState(90);
			func_body();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Func_paraContext extends ParserRuleContext {
		public TerminalNode PARA() { return getToken(BKITParser.PARA, 0); }
		public TerminalNode COLON() { return getToken(BKITParser.COLON, 0); }
		public List<Id_initContext> id_init() {
			return getRuleContexts(Id_initContext.class);
		}
		public Id_initContext id_init(int i) {
			return getRuleContext(Id_initContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(BKITParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(BKITParser.COMMA, i);
		}
		public Func_paraContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_func_para; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).enterFunc_para(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).exitFunc_para(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKITVisitor ) return ((BKITVisitor<? extends T>)visitor).visitFunc_para(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Func_paraContext func_para() throws RecognitionException {
		Func_paraContext _localctx = new Func_paraContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_func_para);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(92);
			match(PARA);
			setState(93);
			match(COLON);
			setState(94);
			id_init();
			setState(99);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(95);
				match(COMMA);
				setState(96);
				id_init();
				}
				}
				setState(101);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Func_bodyContext extends ParserRuleContext {
		public TerminalNode BODY() { return getToken(BKITParser.BODY, 0); }
		public TerminalNode COLON() { return getToken(BKITParser.COLON, 0); }
		public Statement_listContext statement_list() {
			return getRuleContext(Statement_listContext.class,0);
		}
		public TerminalNode ENDBODY() { return getToken(BKITParser.ENDBODY, 0); }
		public TerminalNode DOT() { return getToken(BKITParser.DOT, 0); }
		public Func_bodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_func_body; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).enterFunc_body(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).exitFunc_body(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKITVisitor ) return ((BKITVisitor<? extends T>)visitor).visitFunc_body(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Func_bodyContext func_body() throws RecognitionException {
		Func_bodyContext _localctx = new Func_bodyContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_func_body);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(102);
			match(BODY);
			setState(103);
			match(COLON);
			setState(104);
			statement_list();
			setState(105);
			match(ENDBODY);
			setState(106);
			match(DOT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class All_literalContext extends ParserRuleContext {
		public Single_litContext single_lit() {
			return getRuleContext(Single_litContext.class,0);
		}
		public Array_litContext array_lit() {
			return getRuleContext(Array_litContext.class,0);
		}
		public All_literalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_all_literal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).enterAll_literal(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).exitAll_literal(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKITVisitor ) return ((BKITVisitor<? extends T>)visitor).visitAll_literal(this);
			else return visitor.visitChildren(this);
		}
	}

	public final All_literalContext all_literal() throws RecognitionException {
		All_literalContext _localctx = new All_literalContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_all_literal);
		try {
			setState(110);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INT_LIT:
			case FLOAT_LIT:
			case BOOL_LIT:
			case STRING_LIT:
				enterOuterAlt(_localctx, 1);
				{
				setState(108);
				single_lit();
				}
				break;
			case CBRACKET_L:
				enterOuterAlt(_localctx, 2);
				{
				setState(109);
				array_lit();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Single_litContext extends ParserRuleContext {
		public TerminalNode INT_LIT() { return getToken(BKITParser.INT_LIT, 0); }
		public TerminalNode FLOAT_LIT() { return getToken(BKITParser.FLOAT_LIT, 0); }
		public TerminalNode BOOL_LIT() { return getToken(BKITParser.BOOL_LIT, 0); }
		public TerminalNode STRING_LIT() { return getToken(BKITParser.STRING_LIT, 0); }
		public Single_litContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_single_lit; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).enterSingle_lit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).exitSingle_lit(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKITVisitor ) return ((BKITVisitor<? extends T>)visitor).visitSingle_lit(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Single_litContext single_lit() throws RecognitionException {
		Single_litContext _localctx = new Single_litContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_single_lit);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(112);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INT_LIT) | (1L << FLOAT_LIT) | (1L << BOOL_LIT) | (1L << STRING_LIT))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Array_litContext extends ParserRuleContext {
		public TerminalNode CBRACKET_L() { return getToken(BKITParser.CBRACKET_L, 0); }
		public TerminalNode CBRACKET_R() { return getToken(BKITParser.CBRACKET_R, 0); }
		public List<All_literalContext> all_literal() {
			return getRuleContexts(All_literalContext.class);
		}
		public All_literalContext all_literal(int i) {
			return getRuleContext(All_literalContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(BKITParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(BKITParser.COMMA, i);
		}
		public Array_litContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_array_lit; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).enterArray_lit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).exitArray_lit(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKITVisitor ) return ((BKITVisitor<? extends T>)visitor).visitArray_lit(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Array_litContext array_lit() throws RecognitionException {
		Array_litContext _localctx = new Array_litContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_array_lit);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(114);
			match(CBRACKET_L);
			setState(123);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << CBRACKET_L) | (1L << INT_LIT) | (1L << FLOAT_LIT) | (1L << BOOL_LIT) | (1L << STRING_LIT))) != 0)) {
				{
				setState(115);
				all_literal();
				setState(120);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(116);
					match(COMMA);
					setState(117);
					all_literal();
					}
					}
					setState(122);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(125);
			match(CBRACKET_R);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public List<Exp2Context> exp2() {
			return getRuleContexts(Exp2Context.class);
		}
		public Exp2Context exp2(int i) {
			return getRuleContext(Exp2Context.class,i);
		}
		public TerminalNode EQUAL() { return getToken(BKITParser.EQUAL, 0); }
		public TerminalNode NEQUAL() { return getToken(BKITParser.NEQUAL, 0); }
		public TerminalNode LESS() { return getToken(BKITParser.LESS, 0); }
		public TerminalNode GREATER() { return getToken(BKITParser.GREATER, 0); }
		public TerminalNode SEQUAL() { return getToken(BKITParser.SEQUAL, 0); }
		public TerminalNode LEQUAL() { return getToken(BKITParser.LEQUAL, 0); }
		public TerminalNode NEQUALF() { return getToken(BKITParser.NEQUALF, 0); }
		public TerminalNode LESSF() { return getToken(BKITParser.LESSF, 0); }
		public TerminalNode GREATERF() { return getToken(BKITParser.GREATERF, 0); }
		public TerminalNode SEQUALF() { return getToken(BKITParser.SEQUALF, 0); }
		public TerminalNode LEQUALF() { return getToken(BKITParser.LEQUALF, 0); }
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).exitExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKITVisitor ) return ((BKITVisitor<? extends T>)visitor).visitExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_expression);
		int _la;
		try {
			setState(132);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(127);
				exp2(0);
				setState(128);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQUAL) | (1L << NEQUAL) | (1L << LESS) | (1L << GREATER) | (1L << SEQUAL) | (1L << LEQUAL) | (1L << NEQUALF) | (1L << LESSF) | (1L << GREATERF) | (1L << SEQUALF) | (1L << LEQUALF))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(129);
				exp2(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(131);
				exp2(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Exp2Context extends ParserRuleContext {
		public TerminalNode RBRACKET_L() { return getToken(BKITParser.RBRACKET_L, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RBRACKET_R() { return getToken(BKITParser.RBRACKET_R, 0); }
		public FuncCallContext funcCall() {
			return getRuleContext(FuncCallContext.class,0);
		}
		public Index_expContext index_exp() {
			return getRuleContext(Index_expContext.class,0);
		}
		public List<Exp2Context> exp2() {
			return getRuleContexts(Exp2Context.class);
		}
		public Exp2Context exp2(int i) {
			return getRuleContext(Exp2Context.class,i);
		}
		public TerminalNode SUB() { return getToken(BKITParser.SUB, 0); }
		public TerminalNode SUBF() { return getToken(BKITParser.SUBF, 0); }
		public TerminalNode NOT() { return getToken(BKITParser.NOT, 0); }
		public TerminalNode ID() { return getToken(BKITParser.ID, 0); }
		public All_literalContext all_literal() {
			return getRuleContext(All_literalContext.class,0);
		}
		public TerminalNode MUL() { return getToken(BKITParser.MUL, 0); }
		public TerminalNode MULF() { return getToken(BKITParser.MULF, 0); }
		public TerminalNode DIV() { return getToken(BKITParser.DIV, 0); }
		public TerminalNode DIVF() { return getToken(BKITParser.DIVF, 0); }
		public TerminalNode MOD() { return getToken(BKITParser.MOD, 0); }
		public TerminalNode ADD() { return getToken(BKITParser.ADD, 0); }
		public TerminalNode ADDF() { return getToken(BKITParser.ADDF, 0); }
		public TerminalNode AND() { return getToken(BKITParser.AND, 0); }
		public TerminalNode OR() { return getToken(BKITParser.OR, 0); }
		public Exp2Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exp2; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).enterExp2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).exitExp2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKITVisitor ) return ((BKITVisitor<? extends T>)visitor).visitExp2(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Exp2Context exp2() throws RecognitionException {
		return exp2(0);
	}

	private Exp2Context exp2(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Exp2Context _localctx = new Exp2Context(_ctx, _parentState);
		Exp2Context _prevctx = _localctx;
		int _startState = 22;
		enterRecursionRule(_localctx, 22, RULE_exp2, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(147);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
			case 1:
				{
				setState(135);
				match(RBRACKET_L);
				setState(136);
				expression();
				setState(137);
				match(RBRACKET_R);
				}
				break;
			case 2:
				{
				setState(139);
				funcCall();
				}
				break;
			case 3:
				{
				setState(140);
				index_exp();
				}
				break;
			case 4:
				{
				setState(141);
				_la = _input.LA(1);
				if ( !(_la==SUB || _la==SUBF) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(142);
				exp2(7);
				}
				break;
			case 5:
				{
				{
				setState(143);
				match(NOT);
				}
				setState(144);
				exp2(6);
				}
				break;
			case 6:
				{
				setState(145);
				match(ID);
				}
				break;
			case 7:
				{
				setState(146);
				all_literal();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(160);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,13,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(158);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
					case 1:
						{
						_localctx = new Exp2Context(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_exp2);
						setState(149);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(150);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << MUL) | (1L << MULF) | (1L << DIV) | (1L << DIVF) | (1L << MOD))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(151);
						exp2(6);
						}
						break;
					case 2:
						{
						_localctx = new Exp2Context(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_exp2);
						setState(152);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(153);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ADD) | (1L << ADDF) | (1L << SUB) | (1L << SUBF))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(154);
						exp2(5);
						}
						break;
					case 3:
						{
						_localctx = new Exp2Context(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_exp2);
						setState(155);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(156);
						_la = _input.LA(1);
						if ( !(_la==AND || _la==OR) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(157);
						exp2(4);
						}
						break;
					}
					} 
				}
				setState(162);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,13,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Index_expContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(BKITParser.ID, 0); }
		public FuncCallContext funcCall() {
			return getRuleContext(FuncCallContext.class,0);
		}
		public List<TerminalNode> SBRACKET_L() { return getTokens(BKITParser.SBRACKET_L); }
		public TerminalNode SBRACKET_L(int i) {
			return getToken(BKITParser.SBRACKET_L, i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<TerminalNode> SBRACKET_R() { return getTokens(BKITParser.SBRACKET_R); }
		public TerminalNode SBRACKET_R(int i) {
			return getToken(BKITParser.SBRACKET_R, i);
		}
		public Index_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_index_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).enterIndex_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).exitIndex_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKITVisitor ) return ((BKITVisitor<? extends T>)visitor).visitIndex_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Index_expContext index_exp() throws RecognitionException {
		Index_expContext _localctx = new Index_expContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_index_exp);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(165);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
			case 1:
				{
				setState(163);
				match(ID);
				}
				break;
			case 2:
				{
				setState(164);
				funcCall();
				}
				break;
			}
			setState(171); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(167);
					match(SBRACKET_L);
					setState(168);
					expression();
					setState(169);
					match(SBRACKET_R);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(173); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Statement_listContext extends ParserRuleContext {
		public List<Var_declareContext> var_declare() {
			return getRuleContexts(Var_declareContext.class);
		}
		public Var_declareContext var_declare(int i) {
			return getRuleContext(Var_declareContext.class,i);
		}
		public List<TerminalNode> SEMI() { return getTokens(BKITParser.SEMI); }
		public TerminalNode SEMI(int i) {
			return getToken(BKITParser.SEMI, i);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public Statement_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).enterStatement_list(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).exitStatement_list(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKITVisitor ) return ((BKITVisitor<? extends T>)visitor).visitStatement_list(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Statement_listContext statement_list() throws RecognitionException {
		Statement_listContext _localctx = new Statement_listContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_statement_list);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(180);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==VAR) {
				{
				{
				setState(175);
				var_declare();
				setState(176);
				match(SEMI);
				}
				}
				setState(182);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(186);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(183);
					statement();
					}
					} 
				}
				setState(188);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public TerminalNode SEMI() { return getToken(BKITParser.SEMI, 0); }
		public AssignmentContext assignment() {
			return getRuleContext(AssignmentContext.class,0);
		}
		public TerminalNode BREAK() { return getToken(BKITParser.BREAK, 0); }
		public TerminalNode CONT() { return getToken(BKITParser.CONT, 0); }
		public FuncCallContext funcCall() {
			return getRuleContext(FuncCallContext.class,0);
		}
		public Ret_stmtContext ret_stmt() {
			return getRuleContext(Ret_stmtContext.class,0);
		}
		public If_stmtContext if_stmt() {
			return getRuleContext(If_stmtContext.class,0);
		}
		public For_stmtContext for_stmt() {
			return getRuleContext(For_stmtContext.class,0);
		}
		public While_stmtContext while_stmt() {
			return getRuleContext(While_stmtContext.class,0);
		}
		public Dowhile_stmtContext dowhile_stmt() {
			return getRuleContext(Dowhile_stmtContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).exitStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKITVisitor ) return ((BKITVisitor<? extends T>)visitor).visitStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_statement);
		try {
			setState(203);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ID:
			case BREAK:
			case CONT:
			case RETURN:
				enterOuterAlt(_localctx, 1);
				{
				setState(194);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
				case 1:
					{
					setState(189);
					assignment();
					}
					break;
				case 2:
					{
					setState(190);
					match(BREAK);
					}
					break;
				case 3:
					{
					setState(191);
					match(CONT);
					}
					break;
				case 4:
					{
					setState(192);
					funcCall();
					}
					break;
				case 5:
					{
					setState(193);
					ret_stmt();
					}
					break;
				}
				setState(196);
				match(SEMI);
				}
				break;
			case DO:
			case FOR:
			case IF:
			case WHILE:
				enterOuterAlt(_localctx, 2);
				{
				setState(201);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case IF:
					{
					setState(197);
					if_stmt();
					}
					break;
				case FOR:
					{
					setState(198);
					for_stmt();
					}
					break;
				case WHILE:
					{
					setState(199);
					while_stmt();
					}
					break;
				case DO:
					{
					setState(200);
					dowhile_stmt();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentContext extends ParserRuleContext {
		public TerminalNode ASGN() { return getToken(BKITParser.ASGN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode ID() { return getToken(BKITParser.ID, 0); }
		public Index_expContext index_exp() {
			return getRuleContext(Index_expContext.class,0);
		}
		public AssignmentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignment; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).enterAssignment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).exitAssignment(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKITVisitor ) return ((BKITVisitor<? extends T>)visitor).visitAssignment(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AssignmentContext assignment() throws RecognitionException {
		AssignmentContext _localctx = new AssignmentContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_assignment);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(207);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
			case 1:
				{
				setState(205);
				match(ID);
				}
				break;
			case 2:
				{
				setState(206);
				index_exp();
				}
				break;
			}
			setState(209);
			match(ASGN);
			setState(210);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_stmtContext extends ParserRuleContext {
		public TerminalNode IF() { return getToken(BKITParser.IF, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<TerminalNode> THEN() { return getTokens(BKITParser.THEN); }
		public TerminalNode THEN(int i) {
			return getToken(BKITParser.THEN, i);
		}
		public List<Statement_listContext> statement_list() {
			return getRuleContexts(Statement_listContext.class);
		}
		public Statement_listContext statement_list(int i) {
			return getRuleContext(Statement_listContext.class,i);
		}
		public TerminalNode ENDIF() { return getToken(BKITParser.ENDIF, 0); }
		public TerminalNode DOT() { return getToken(BKITParser.DOT, 0); }
		public List<TerminalNode> ELSEIF() { return getTokens(BKITParser.ELSEIF); }
		public TerminalNode ELSEIF(int i) {
			return getToken(BKITParser.ELSEIF, i);
		}
		public TerminalNode ELSE() { return getToken(BKITParser.ELSE, 0); }
		public If_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_if_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).enterIf_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).exitIf_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKITVisitor ) return ((BKITVisitor<? extends T>)visitor).visitIf_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final If_stmtContext if_stmt() throws RecognitionException {
		If_stmtContext _localctx = new If_stmtContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_if_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(212);
			match(IF);
			setState(213);
			expression();
			setState(214);
			match(THEN);
			setState(215);
			statement_list();
			setState(223);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ELSEIF) {
				{
				{
				setState(216);
				match(ELSEIF);
				setState(217);
				expression();
				setState(218);
				match(THEN);
				setState(219);
				statement_list();
				}
				}
				setState(225);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(228);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ELSE) {
				{
				setState(226);
				match(ELSE);
				setState(227);
				statement_list();
				}
			}

			setState(230);
			match(ENDIF);
			setState(231);
			match(DOT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class For_stmtContext extends ParserRuleContext {
		public TerminalNode FOR() { return getToken(BKITParser.FOR, 0); }
		public TerminalNode RBRACKET_L() { return getToken(BKITParser.RBRACKET_L, 0); }
		public TerminalNode ID() { return getToken(BKITParser.ID, 0); }
		public TerminalNode ASGN() { return getToken(BKITParser.ASGN, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(BKITParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(BKITParser.COMMA, i);
		}
		public TerminalNode RBRACKET_R() { return getToken(BKITParser.RBRACKET_R, 0); }
		public TerminalNode DO() { return getToken(BKITParser.DO, 0); }
		public Statement_listContext statement_list() {
			return getRuleContext(Statement_listContext.class,0);
		}
		public TerminalNode ENDFOR() { return getToken(BKITParser.ENDFOR, 0); }
		public TerminalNode DOT() { return getToken(BKITParser.DOT, 0); }
		public For_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_for_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).enterFor_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).exitFor_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKITVisitor ) return ((BKITVisitor<? extends T>)visitor).visitFor_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final For_stmtContext for_stmt() throws RecognitionException {
		For_stmtContext _localctx = new For_stmtContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_for_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(233);
			match(FOR);
			setState(234);
			match(RBRACKET_L);
			setState(235);
			match(ID);
			setState(236);
			match(ASGN);
			setState(237);
			expression();
			setState(238);
			match(COMMA);
			setState(239);
			expression();
			setState(240);
			match(COMMA);
			setState(241);
			expression();
			setState(242);
			match(RBRACKET_R);
			setState(243);
			match(DO);
			setState(244);
			statement_list();
			setState(245);
			match(ENDFOR);
			setState(246);
			match(DOT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class While_stmtContext extends ParserRuleContext {
		public TerminalNode WHILE() { return getToken(BKITParser.WHILE, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode DO() { return getToken(BKITParser.DO, 0); }
		public Statement_listContext statement_list() {
			return getRuleContext(Statement_listContext.class,0);
		}
		public TerminalNode ENDWHILE() { return getToken(BKITParser.ENDWHILE, 0); }
		public TerminalNode DOT() { return getToken(BKITParser.DOT, 0); }
		public While_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_while_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).enterWhile_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).exitWhile_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKITVisitor ) return ((BKITVisitor<? extends T>)visitor).visitWhile_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final While_stmtContext while_stmt() throws RecognitionException {
		While_stmtContext _localctx = new While_stmtContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_while_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(248);
			match(WHILE);
			setState(249);
			expression();
			setState(250);
			match(DO);
			setState(251);
			statement_list();
			setState(252);
			match(ENDWHILE);
			setState(253);
			match(DOT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Dowhile_stmtContext extends ParserRuleContext {
		public TerminalNode DO() { return getToken(BKITParser.DO, 0); }
		public Statement_listContext statement_list() {
			return getRuleContext(Statement_listContext.class,0);
		}
		public TerminalNode WHILE() { return getToken(BKITParser.WHILE, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode ENDDO() { return getToken(BKITParser.ENDDO, 0); }
		public TerminalNode DOT() { return getToken(BKITParser.DOT, 0); }
		public Dowhile_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dowhile_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).enterDowhile_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).exitDowhile_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKITVisitor ) return ((BKITVisitor<? extends T>)visitor).visitDowhile_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Dowhile_stmtContext dowhile_stmt() throws RecognitionException {
		Dowhile_stmtContext _localctx = new Dowhile_stmtContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_dowhile_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(255);
			match(DO);
			setState(256);
			statement_list();
			setState(257);
			match(WHILE);
			setState(258);
			expression();
			setState(259);
			match(ENDDO);
			setState(260);
			match(DOT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FuncCallContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(BKITParser.ID, 0); }
		public TerminalNode RBRACKET_L() { return getToken(BKITParser.RBRACKET_L, 0); }
		public TerminalNode RBRACKET_R() { return getToken(BKITParser.RBRACKET_R, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(BKITParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(BKITParser.COMMA, i);
		}
		public FuncCallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_funcCall; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).enterFuncCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).exitFuncCall(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKITVisitor ) return ((BKITVisitor<? extends T>)visitor).visitFuncCall(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FuncCallContext funcCall() throws RecognitionException {
		FuncCallContext _localctx = new FuncCallContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_funcCall);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(262);
			match(ID);
			setState(263);
			match(RBRACKET_L);
			setState(272);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ID) | (1L << SUB) | (1L << SUBF) | (1L << NOT) | (1L << RBRACKET_L) | (1L << CBRACKET_L) | (1L << INT_LIT) | (1L << FLOAT_LIT) | (1L << BOOL_LIT) | (1L << STRING_LIT))) != 0)) {
				{
				setState(264);
				expression();
				setState(269);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(265);
					match(COMMA);
					setState(266);
					expression();
					}
					}
					setState(271);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(274);
			match(RBRACKET_R);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Ret_stmtContext extends ParserRuleContext {
		public TerminalNode RETURN() { return getToken(BKITParser.RETURN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Ret_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ret_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).enterRet_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).exitRet_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKITVisitor ) return ((BKITVisitor<? extends T>)visitor).visitRet_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Ret_stmtContext ret_stmt() throws RecognitionException {
		Ret_stmtContext _localctx = new Ret_stmtContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_ret_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(276);
			match(RETURN);
			setState(278);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ID) | (1L << SUB) | (1L << SUBF) | (1L << NOT) | (1L << RBRACKET_L) | (1L << CBRACKET_L) | (1L << INT_LIT) | (1L << FLOAT_LIT) | (1L << BOOL_LIT) | (1L << STRING_LIT))) != 0)) {
				{
				setState(277);
				expression();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 11:
			return exp2_sempred((Exp2Context)_localctx, predIndex);
		}
		return true;
	}
	private boolean exp2_sempred(Exp2Context _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 5);
		case 1:
			return precpred(_ctx, 4);
		case 2:
			return precpred(_ctx, 3);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3C\u011b\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\3\2\3\2\3\2\7\2\62"+
		"\n\2\f\2\16\2\65\13\2\3\2\7\28\n\2\f\2\16\2;\13\2\3\2\3\2\3\3\3\3\3\3"+
		"\3\3\3\3\7\3D\n\3\f\3\16\3G\13\3\3\4\3\4\3\4\5\4L\n\4\3\5\3\5\3\5\3\5"+
		"\7\5R\n\5\f\5\16\5U\13\5\3\6\3\6\3\6\3\6\5\6[\n\6\3\6\3\6\3\7\3\7\3\7"+
		"\3\7\3\7\7\7d\n\7\f\7\16\7g\13\7\3\b\3\b\3\b\3\b\3\b\3\b\3\t\3\t\5\tq"+
		"\n\t\3\n\3\n\3\13\3\13\3\13\3\13\7\13y\n\13\f\13\16\13|\13\13\5\13~\n"+
		"\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\5\f\u0087\n\f\3\r\3\r\3\r\3\r\3\r\3"+
		"\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\5\r\u0096\n\r\3\r\3\r\3\r\3\r\3\r\3\r\3"+
		"\r\3\r\3\r\7\r\u00a1\n\r\f\r\16\r\u00a4\13\r\3\16\3\16\5\16\u00a8\n\16"+
		"\3\16\3\16\3\16\3\16\6\16\u00ae\n\16\r\16\16\16\u00af\3\17\3\17\3\17\7"+
		"\17\u00b5\n\17\f\17\16\17\u00b8\13\17\3\17\7\17\u00bb\n\17\f\17\16\17"+
		"\u00be\13\17\3\20\3\20\3\20\3\20\3\20\5\20\u00c5\n\20\3\20\3\20\3\20\3"+
		"\20\3\20\5\20\u00cc\n\20\5\20\u00ce\n\20\3\21\3\21\5\21\u00d2\n\21\3\21"+
		"\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\7\22\u00e0\n\22"+
		"\f\22\16\22\u00e3\13\22\3\22\3\22\5\22\u00e7\n\22\3\22\3\22\3\22\3\23"+
		"\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23"+
		"\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\25\3\25\3\25\3\25\3\25\3\25\3\25"+
		"\3\26\3\26\3\26\3\26\3\26\7\26\u010e\n\26\f\26\16\26\u0111\13\26\5\26"+
		"\u0113\n\26\3\26\3\26\3\27\3\27\5\27\u0119\n\27\3\27\2\3\30\30\2\4\6\b"+
		"\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,\2\b\3\2:=\3\2%/\3\2\32\33\3\2"+
		"\34 \3\2\30\33\3\2#$\2\u012a\2\63\3\2\2\2\4>\3\2\2\2\6H\3\2\2\2\bM\3\2"+
		"\2\2\nV\3\2\2\2\f^\3\2\2\2\16h\3\2\2\2\20p\3\2\2\2\22r\3\2\2\2\24t\3\2"+
		"\2\2\26\u0086\3\2\2\2\30\u0095\3\2\2\2\32\u00a7\3\2\2\2\34\u00b6\3\2\2"+
		"\2\36\u00cd\3\2\2\2 \u00d1\3\2\2\2\"\u00d6\3\2\2\2$\u00eb\3\2\2\2&\u00fa"+
		"\3\2\2\2(\u0101\3\2\2\2*\u0108\3\2\2\2,\u0116\3\2\2\2./\5\4\3\2/\60\7"+
		"\64\2\2\60\62\3\2\2\2\61.\3\2\2\2\62\65\3\2\2\2\63\61\3\2\2\2\63\64\3"+
		"\2\2\2\649\3\2\2\2\65\63\3\2\2\2\668\5\n\6\2\67\66\3\2\2\28;\3\2\2\29"+
		"\67\3\2\2\29:\3\2\2\2:<\3\2\2\2;9\3\2\2\2<=\7\2\2\3=\3\3\2\2\2>?\7\25"+
		"\2\2?@\7\67\2\2@E\5\6\4\2AB\7\66\2\2BD\5\6\4\2CA\3\2\2\2DG\3\2\2\2EC\3"+
		"\2\2\2EF\3\2\2\2F\5\3\2\2\2GE\3\2\2\2HK\5\b\5\2IJ\7!\2\2JL\5\20\t\2KI"+
		"\3\2\2\2KL\3\2\2\2L\7\3\2\2\2MS\7\4\2\2NO\7\62\2\2OP\7:\2\2PR\7\63\2\2"+
		"QN\3\2\2\2RU\3\2\2\2SQ\3\2\2\2ST\3\2\2\2T\t\3\2\2\2US\3\2\2\2VW\7\20\2"+
		"\2WX\7\67\2\2XZ\7\4\2\2Y[\5\f\7\2ZY\3\2\2\2Z[\3\2\2\2[\\\3\2\2\2\\]\5"+
		"\16\b\2]\13\3\2\2\2^_\7\22\2\2_`\7\67\2\2`e\5\6\4\2ab\7\66\2\2bd\5\6\4"+
		"\2ca\3\2\2\2dg\3\2\2\2ec\3\2\2\2ef\3\2\2\2f\r\3\2\2\2ge\3\2\2\2hi\7\5"+
		"\2\2ij\7\67\2\2jk\5\34\17\2kl\7\13\2\2lm\7\65\2\2m\17\3\2\2\2nq\5\22\n"+
		"\2oq\5\24\13\2pn\3\2\2\2po\3\2\2\2q\21\3\2\2\2rs\t\2\2\2s\23\3\2\2\2t"+
		"}\78\2\2uz\5\20\t\2vw\7\66\2\2wy\5\20\t\2xv\3\2\2\2y|\3\2\2\2zx\3\2\2"+
		"\2z{\3\2\2\2{~\3\2\2\2|z\3\2\2\2}u\3\2\2\2}~\3\2\2\2~\177\3\2\2\2\177"+
		"\u0080\79\2\2\u0080\25\3\2\2\2\u0081\u0082\5\30\r\2\u0082\u0083\t\3\2"+
		"\2\u0083\u0084\5\30\r\2\u0084\u0087\3\2\2\2\u0085\u0087\5\30\r\2\u0086"+
		"\u0081\3\2\2\2\u0086\u0085\3\2\2\2\u0087\27\3\2\2\2\u0088\u0089\b\r\1"+
		"\2\u0089\u008a\7\60\2\2\u008a\u008b\5\26\f\2\u008b\u008c\7\61\2\2\u008c"+
		"\u0096\3\2\2\2\u008d\u0096\5*\26\2\u008e\u0096\5\32\16\2\u008f\u0090\t"+
		"\4\2\2\u0090\u0096\5\30\r\t\u0091\u0092\7\"\2\2\u0092\u0096\5\30\r\b\u0093"+
		"\u0096\7\4\2\2\u0094\u0096\5\20\t\2\u0095\u0088\3\2\2\2\u0095\u008d\3"+
		"\2\2\2\u0095\u008e\3\2\2\2\u0095\u008f\3\2\2\2\u0095\u0091\3\2\2\2\u0095"+
		"\u0093\3\2\2\2\u0095\u0094\3\2\2\2\u0096\u00a2\3\2\2\2\u0097\u0098\f\7"+
		"\2\2\u0098\u0099\t\5\2\2\u0099\u00a1\5\30\r\b\u009a\u009b\f\6\2\2\u009b"+
		"\u009c\t\6\2\2\u009c\u00a1\5\30\r\7\u009d\u009e\f\5\2\2\u009e\u009f\t"+
		"\7\2\2\u009f\u00a1\5\30\r\6\u00a0\u0097\3\2\2\2\u00a0\u009a\3\2\2\2\u00a0"+
		"\u009d\3\2\2\2\u00a1\u00a4\3\2\2\2\u00a2\u00a0\3\2\2\2\u00a2\u00a3\3\2"+
		"\2\2\u00a3\31\3\2\2\2\u00a4\u00a2\3\2\2\2\u00a5\u00a8\7\4\2\2\u00a6\u00a8"+
		"\5*\26\2\u00a7\u00a5\3\2\2\2\u00a7\u00a6\3\2\2\2\u00a8\u00ad\3\2\2\2\u00a9"+
		"\u00aa\7\62\2\2\u00aa\u00ab\5\26\f\2\u00ab\u00ac\7\63\2\2\u00ac\u00ae"+
		"\3\2\2\2\u00ad\u00a9\3\2\2\2\u00ae\u00af\3\2\2\2\u00af\u00ad\3\2\2\2\u00af"+
		"\u00b0\3\2\2\2\u00b0\33\3\2\2\2\u00b1\u00b2\5\4\3\2\u00b2\u00b3\7\64\2"+
		"\2\u00b3\u00b5\3\2\2\2\u00b4\u00b1\3\2\2\2\u00b5\u00b8\3\2\2\2\u00b6\u00b4"+
		"\3\2\2\2\u00b6\u00b7\3\2\2\2\u00b7\u00bc\3\2\2\2\u00b8\u00b6\3\2\2\2\u00b9"+
		"\u00bb\5\36\20\2\u00ba\u00b9\3\2\2\2\u00bb\u00be\3\2\2\2\u00bc\u00ba\3"+
		"\2\2\2\u00bc\u00bd\3\2\2\2\u00bd\35\3\2\2\2\u00be\u00bc\3\2\2\2\u00bf"+
		"\u00c5\5 \21\2\u00c0\u00c5\7\6\2\2\u00c1\u00c5\7\7\2\2\u00c2\u00c5\5*"+
		"\26\2\u00c3\u00c5\5,\27\2\u00c4\u00bf\3\2\2\2\u00c4\u00c0\3\2\2\2\u00c4"+
		"\u00c1\3\2\2\2\u00c4\u00c2\3\2\2\2\u00c4\u00c3\3\2\2\2\u00c5\u00c6\3\2"+
		"\2\2\u00c6\u00ce\7\64\2\2\u00c7\u00cc\5\"\22\2\u00c8\u00cc\5$\23\2\u00c9"+
		"\u00cc\5&\24\2\u00ca\u00cc\5(\25\2\u00cb\u00c7\3\2\2\2\u00cb\u00c8\3\2"+
		"\2\2\u00cb\u00c9\3\2\2\2\u00cb\u00ca\3\2\2\2\u00cc\u00ce\3\2\2\2\u00cd"+
		"\u00c4\3\2\2\2\u00cd\u00cb\3\2\2\2\u00ce\37\3\2\2\2\u00cf\u00d2\7\4\2"+
		"\2\u00d0\u00d2\5\32\16\2\u00d1\u00cf\3\2\2\2\u00d1\u00d0\3\2\2\2\u00d2"+
		"\u00d3\3\2\2\2\u00d3\u00d4\7!\2\2\u00d4\u00d5\5\26\f\2\u00d5!\3\2\2\2"+
		"\u00d6\u00d7\7\21\2\2\u00d7\u00d8\5\26\f\2\u00d8\u00d9\7\24\2\2\u00d9"+
		"\u00e1\5\34\17\2\u00da\u00db\7\n\2\2\u00db\u00dc\5\26\f\2\u00dc\u00dd"+
		"\7\24\2\2\u00dd\u00de\5\34\17\2\u00de\u00e0\3\2\2\2\u00df\u00da\3\2\2"+
		"\2\u00e0\u00e3\3\2\2\2\u00e1\u00df\3\2\2\2\u00e1\u00e2\3\2\2\2\u00e2\u00e6"+
		"\3\2\2\2\u00e3\u00e1\3\2\2\2\u00e4\u00e5\7\t\2\2\u00e5\u00e7\5\34\17\2"+
		"\u00e6\u00e4\3\2\2\2\u00e6\u00e7\3\2\2\2\u00e7\u00e8\3\2\2\2\u00e8\u00e9"+
		"\7\f\2\2\u00e9\u00ea\7\65\2\2\u00ea#\3\2\2\2\u00eb\u00ec\7\17\2\2\u00ec"+
		"\u00ed\7\60\2\2\u00ed\u00ee\7\4\2\2\u00ee\u00ef\7!\2\2\u00ef\u00f0\5\26"+
		"\f\2\u00f0\u00f1\7\66\2\2\u00f1\u00f2\5\26\f\2\u00f2\u00f3\7\66\2\2\u00f3"+
		"\u00f4\5\26\f\2\u00f4\u00f5\7\61\2\2\u00f5\u00f6\7\b\2\2\u00f6\u00f7\5"+
		"\34\17\2\u00f7\u00f8\7\r\2\2\u00f8\u00f9\7\65\2\2\u00f9%\3\2\2\2\u00fa"+
		"\u00fb\7\26\2\2\u00fb\u00fc\5\26\f\2\u00fc\u00fd\7\b\2\2\u00fd\u00fe\5"+
		"\34\17\2\u00fe\u00ff\7\16\2\2\u00ff\u0100\7\65\2\2\u0100\'\3\2\2\2\u0101"+
		"\u0102\7\b\2\2\u0102\u0103\5\34\17\2\u0103\u0104\7\26\2\2\u0104\u0105"+
		"\5\26\f\2\u0105\u0106\7\27\2\2\u0106\u0107\7\65\2\2\u0107)\3\2\2\2\u0108"+
		"\u0109\7\4\2\2\u0109\u0112\7\60\2\2\u010a\u010f\5\26\f\2\u010b\u010c\7"+
		"\66\2\2\u010c\u010e\5\26\f\2\u010d\u010b\3\2\2\2\u010e\u0111\3\2\2\2\u010f"+
		"\u010d\3\2\2\2\u010f\u0110\3\2\2\2\u0110\u0113\3\2\2\2\u0111\u010f\3\2"+
		"\2\2\u0112\u010a\3\2\2\2\u0112\u0113\3\2\2\2\u0113\u0114\3\2\2\2\u0114"+
		"\u0115\7\61\2\2\u0115+\3\2\2\2\u0116\u0118\7\23\2\2\u0117\u0119\5\26\f"+
		"\2\u0118\u0117\3\2\2\2\u0118\u0119\3\2\2\2\u0119-\3\2\2\2\35\639EKSZe"+
		"pz}\u0086\u0095\u00a0\u00a2\u00a7\u00af\u00b6\u00bc\u00c4\u00cb\u00cd"+
		"\u00d1\u00e1\u00e6\u010f\u0112\u0118";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}