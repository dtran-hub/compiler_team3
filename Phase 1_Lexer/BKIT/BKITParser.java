// Generated from BKIT.g4 by ANTLR 4.9.2


 
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BKITParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.9.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		COMMENT=1, ID=2, BODY=3, BREAK=4, CONT=5, DO=6, ELSE=7, ELSEIF=8, ENDBODY=9, 
		ENDIF=10, ENDFOR=11, ENDWHILE=12, FOR=13, FUNC=14, IF=15, PARA=16, RETURN=17, 
		THEN=18, VAR=19, WHILE=20, ENDDO=21, ADD=22, ADDF=23, SUB=24, SUBF=25, 
		MUL=26, MULF=27, DIV=28, DIVF=29, MOD=30, NOT=31, AND=32, OR=33, EQUAL=34, 
		NEQUAL=35, SMALLER=36, LARGER=37, SEQUAL=38, LEQUAL=39, NEQUALF=40, SMALLERF=41, 
		LARGERF=42, SEQUALF=43, LEQUALF=44, ASGN=45, RBRACKET_L=46, RBRACKET_R=47, 
		SBRACKET_L=48, SBRACKET_R=49, SEMI=50, DOT=51, COMMA=52, COLON=53, CBRACKET_L=54, 
		CBRACKET_R=55, INT_LIT=56, FLOAT_LIT=57, BOOL_LIT=58, STRING_LIT=59, WS=60, 
		UNCLOSE_STRING=61, ILLEGAL_ESCAPE=62, UNTERMINATED_COMMENT=63, ARRAY_LIT=64;
	public static final int
		RULE_program = 0;
	private static String[] makeRuleNames() {
		return new String[] {
			"program"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, "'Body'", "'Break'", "'Continue'", "'Do'", "'Else'", 
			"'ElseIf'", "'EndBody'", "'EndIf'", "'EndFor'", "'EndWhile'", "'For'", 
			"'Function'", "'If'", "'Parameter'", "'Return'", "'Then'", "'Var'", "'While'", 
			"'EndDo'", "'+'", "'+.'", "'-'", "'-.'", "'*'", "'*.'", "'\\'", "'\\.'", 
			"'%'", "'!'", "'&&'", "'||'", "'=='", "'!='", "'<'", "'>'", "'<='", "'>='", 
			"'=/='", "'<.'", "'>.'", "'<=.'", "'>=.'", "'='", "'('", "')'", "'['", 
			"']'", "';'", "'.'", "','", "':'", "'{'", "'}'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "COMMENT", "ID", "BODY", "BREAK", "CONT", "DO", "ELSE", "ELSEIF", 
			"ENDBODY", "ENDIF", "ENDFOR", "ENDWHILE", "FOR", "FUNC", "IF", "PARA", 
			"RETURN", "THEN", "VAR", "WHILE", "ENDDO", "ADD", "ADDF", "SUB", "SUBF", 
			"MUL", "MULF", "DIV", "DIVF", "MOD", "NOT", "AND", "OR", "EQUAL", "NEQUAL", 
			"SMALLER", "LARGER", "SEQUAL", "LEQUAL", "NEQUALF", "SMALLERF", "LARGERF", 
			"SEQUALF", "LEQUALF", "ASGN", "RBRACKET_L", "RBRACKET_R", "SBRACKET_L", 
			"SBRACKET_R", "SEMI", "DOT", "COMMA", "COLON", "CBRACKET_L", "CBRACKET_R", 
			"INT_LIT", "FLOAT_LIT", "BOOL_LIT", "STRING_LIT", "WS", "UNCLOSE_STRING", 
			"ILLEGAL_ESCAPE", "UNTERMINATED_COMMENT", "ARRAY_LIT"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "BKIT.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public BKITParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ProgramContext extends ParserRuleContext {
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BKITListener ) ((BKITListener)listener).exitProgram(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		try {
			enterOuterAlt(_localctx, 1);
			{
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3B\7\4\2\t\2\3\2\3"+
		"\2\3\2\2\2\3\2\2\2\2\5\2\4\3\2\2\2\4\5\3\2\2\2\5\3\3\2\2\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}